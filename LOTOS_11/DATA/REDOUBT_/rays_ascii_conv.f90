character*8 mdlnm
character*3 mdl_typ
character iter
integer it
real rays_best(3,500)
write(*,*) "Enter model name"
read(*,*) mdlnm
write(*,*) "Enter model type (Real or Syn)"
read(*,*) mdl_typ 
write(*,*) "Enter iteration number"
read(*,*) iter

write(*,*) 'Mdl= ',mdlnm,', mdl_typ= ',mdl_typ,', iter= ',iter

! for getting all the information about raypaths for any iteration of the inversion. The files raypaths.dat file has to be saved in
! the TMP_files folder
  if (mdl_typ.eq."Syn" .and. iter.eq."*") then
    open(11,file='./'//mdlnm//'/data/rays_syn_ascii.dat')
    open(1,file='./'//mdlnm//'/data/rays_syn.dat',form='unformatted')
  else
    open(11,file='./'//mdlnm//'/data/rays'//iter//'_ascii.dat')
    open(1,file='./'//mdlnm//'/data/rays'//iter//'.dat',form='unformatted')
  end if
  !open(12,file='../../TMP_files/tmp/ray_paths_5.dat',form='unformatted')
21 continue
  read(1,end=22)xini,yini,zini,nkrat,uidx
  write(11,*)xini,yini,zini,nkrat,uidx
  !write(*,*) xini,yini,zini,nkrat
  do j=1,nkrat
    read(1)ips,ist,tobs_old,tref
    write(11,*)ips,ist,tobs_old,tref
    !write(*,*)ips,ist,tobs_old,tref
    !*****************uncomment these lines for getting raypath nodes also***********************************************
    !read(12)nod_best
    !write(11,*) nod_best
    !do inod=1,nod_best
    !  read(12) (rays_best(i3,inod),i3=1,3)
    !end do
    !do inod=1,nod_best
    !  write(11,*) rays_best(:,inod)
    !end do
    !********************************************************************************************************************
  end do
  goto 21
  close(12)
22 close(1)
  close(11)
end

