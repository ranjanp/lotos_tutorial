import numpy as np
from matplotlib import pyplot as plt
import matplotlib as mpl

mdlnm=input('Enter Model Name: ')

count=0
velp_old=[]
vels_old=[]
dep_old=[]
with open('./'+mdlnm+'/data/ref_start.dat','r') as f:
    for line in f:
        arr = line.split('\t')
        count=count+1
        if(count>1):
            dep_old.append(np.float32(arr[0]))
            velp_old.append(np.float32(arr[1]))
            vels_old.append(np.float32(arr[2]))
        else:
            vpvs_old=np.float32(arr[0])

f.close()

dep_old = np.array(dep_old)
velp_old = np.array(velp_old)
vels_old = np.array(vels_old)

if(vpvs_old>0):
    vels_old = velp_old/vpvs_old

print("Starting 1D velocity model \n")
print("Depth array : ",dep_old)
print("P-wave velocity : ",velp_old)
print("S-wave velocity : ",vels_old)
print("Vp/Vs value : ",vpvs_old)


count=0
velp_new=[]
vels_new=[]
dep_new=[]
with open('./'+mdlnm+'/data/refmod.dat','r') as f:
    for line in f:
        arr = line.split()
        count=count+1
        if(count>1):
            dep_new.append(np.float32(arr[0]))
            velp_new.append(np.float32(arr[1]))
            vels_new.append(np.float32(arr[2]))
        else:
            vpvs_new=np.float32(arr[0])

f.close()

dep_new = np.array(dep_new)
velp_new=np.array(velp_new)
vels_new=np.array(vels_new)

if(vpvs_new>0):
    vels_new = velp_new/vpvs_new

print("\nOptimized 1D velocity model \n")
print("Depth array : ",dep_new)
print("P-wave velocity : ",velp_new)
print("S-wave velocity : ",vels_new)
print("Vp/Vs value : ",vpvs_new)


mpl.rcParams.update({'font.size': 14})

plt.figure(1)
plt.plot(velp_old,dep_old,color='red',alpha=0.3,linestyle='--')
plt.plot(vels_old,dep_old,color='blue',alpha=0.3,linestyle='--')
plt.plot(velp_new,dep_new,color='red')
plt.plot(vels_new,dep_new,color='blue')
plt.gca().invert_yaxis()
plt.xlabel('Velocity (in km/s)')
plt.ylabel('Depth (in km)')
plt.ylim([100,0])
plt.show()
