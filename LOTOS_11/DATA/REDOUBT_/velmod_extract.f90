allocatable  xtop_p(:,:), ztop_p(:,:), kpop_p(:,:)
allocatable dv_3d(:,:,:)
integer nt_p(200),npz_p(10),igr,igno
real ylev_p(200),p_strng,s_strng
real*8 tmp
character*8 line,md,p_char,s_char,st1,st2,st3
character*80 fmt1 
character it,gr,ps,wve

!write(*,*) "Enter model name"
read(*,*) md

!write(*,*) "Enter iteration number"
read(*,*) it

!write(*,*) "Enter number of grids"
read(*,*) igno

open(1,file='./'//md//'/MAJOR_PARAM.DAT',action='read')
do i=1,10000
	read(1,'(a8)',end=573)line
	if(line.eq.'INVERSIO') goto 574
end do
573 continue
write(*,*)' cannot find INVERSION PARAMETERS in MAJOR_PARAM.DAT!!!'
pause
574 continue
read(1,*)iter_lsqr
read(1,*)wg_vel_p,wg_vel_s
read(1,*)sm_vel_p,sm_vel_s
read(1,*)rg_vel_p,rg_vel_s
read(1,*)
read(1,*)wg_st_p,wg_st_s
read(1,*)wzt_hor
read(1,*)wzt_ver
read(1,*)wzt_time
close(1)

do ips=1,2
  write(ps,'(i1)') ips
  tmp=0
  icount=0
  do igr=1,igno
    write(gr,'(i1)') igr
    open(1,file='./'//md//'/data/obr'//ps//gr//'.dat')
    read(1,*) nvel_p
    close(1)

    open(1,file='./'//md//'/data/levinfo'//ps//gr//'.dat')
    i=0
    722 i=i+1
      read(1,*,end=721)nt,ylev_p(i)
      goto 722
    721 ny_p=i-1
    close(1)
  
    open(1,file='./'//md//'/data/gr'//ps//gr//'.dat')
    nmax=0
    do n=1,ny_p
      read(1,*)nt
      !write(*,*)nt
      if(nt.gt.nmax) nmax=nt
      do i=1,nt
        read(1,*)xxx,zzz,lll
      end do
    end do
    close(1)
  
    allocate(xtop_p(nmax,ny_p),ztop_p(nmax,ny_p),kpop_p(nmax,ny_p))
  
    open(11,file='./'//md//'/nodes_xyz_'//ps//gr//'.dat')
  
    open(1,file='./'//md//'/data/gr'//ps//gr//'.dat')
    npz_p=0
    do n=1,ny_p
      read(1,*)nt
      !write(*,*)n,' ntop=',nt
      nt_p(n)=nt
      do i=1,nt_p(n)
        read(1,*)xtop_p(i,n),ztop_p(i,n),kpop_p(i,n),izone
        if (kpop_p(i,n).gt.0) write(11,*) xtop_p(i,n),ylev_p(n),ztop_p(i,n)
        !if(it_curr.ne.1)write(*,*)xtop(i,n),ztop(i,n),popor(i,n),izone
        if(kpop_p(i,n).eq.0) cycle
        if(izone.ne.0) npz_p(izone)=npz_p(izone)+1
        imatr=kpop_p(i,n)
      end do
    end do
    close(1)
    close(11)

    if(ips.eq.1) wve="p"
    if(ips.eq.2) wve="s"
    open(11,file='./'//md//'/vel_'//wve//'_'//it//gr//'_full.dat')
    open(12,file='./'//md//'/nodes_xyz_'//ps//gr//'.dat')
    open(1,file='./'//md//'/data/vel_'//wve//'_'//it//gr//'.dat')
    do i=1,nvel_p
      read(1,*)dvp_old,vvp,izone     
      read(12,*)xx,yy,zz 
      write(11,*)xx,yy,zz,dvp_old,vvp  
      tmp = tmp + dvp_old*dvp_old
    end do
    close(1)
    close(11)
    close(12)
    icount = icount + nvel_p   
    deallocate(xtop_p,ztop_p,kpop_p)
  end do
  avg_err = sqrt(tmp)
  open(1,file='./'//md//'/avg_norm_'//ps//it//'.dat',action='write')
  open(2,file='./'//md//'/info_resid.dat',action='read')
  read(it,'(i4)') iter
  read(ps,'(i4)') ip_s 
  !write(*,*) iter, ip_s
  p_char="dtot_p"
  s_char="dtot_s"

  843 continue 
    read(2,*,end=844) st1, itr, st2, vr_p, st3, vr2
    read(2,*,end=844) st1, itr, st2, vr_s, st3, vr2
    read(2,*)
    if (itr.eq.iter) then
       if (ip_s.eq.1) then
          !write(1,*) 'wgt_mdl wgt_smth wgt_dmp wgt_stcr wgt_src_shft_hor wgt_src_shft_ver wgt_src_shft_time'//  &
          !           &' mdl_norm rms_resid mdl_typ' 
          fmt1='(F6.3,4x,F6.3,4x,F6.3,4x,F6.3,4x,F6.3,4x,F6.3,4x,F6.3,4x,F15.8,4x,F15.8,4x,A)'
          write(1,fmt1) wg_vel_p, sm_vel_p, rg_vel_p, wg_st_p, wzt_hor, wzt_ver, wzt_time, sqrt(tmp/icount), vr_p, ps
       else if (ip_s.eq.2) then
          !write(1,*) 'wgt_mdl wgt_smth wgt_dmp wgt_stcr wgt_src_shft_hor wgt_src_shft_ver wgt_src_shft_time'//  &
          !           &' mdl_norm rms_resid mdl_typ' 
          write(1,fmt1) wg_vel_s, sm_vel_s, rg_vel_s, wg_st_s, wzt_hor, wzt_ver, wzt_time, sqrt(tmp/icount), vr_s, ps
       else
          continue
       end if
    else
       goto 843
    end if
  844 close(2)
  close(1)
end do


stop
end
