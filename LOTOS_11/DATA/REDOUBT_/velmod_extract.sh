#!/bin/bash

flnm="MODEL_[0-9][0-9]"
num_iter="3"
num_grd="4"

for i in `find ./ -maxdepth 1 -name "$flnm"`;
do
echo ${i//\.\/}
./velmod_extract.exe <<EOF
${i//\.\/}
${num_iter}
${num_grd}
EOF
done
