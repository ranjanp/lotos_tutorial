import numpy as np
from matplotlib import pyplot as plt
from matplotlib import rc 
from glob import glob
import os

font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 16}

rc('font', **font)

mdl=input("Enter Model Name Type (Example: Mavr3lw*): ")
itr=input("Enter Number of iterations used: ")
p_s=input("Enter 1 for P or 2 for S model: ")
plt_typ=input("Enter Parameter for plotting (Smoothing(s), Damping(d), St. Correction(st)," \
		+"Source Shift Weights (Horizontal Weight(hw), Vertical Weight(vw), Temporal Weight(tw)) ): ")
curv_typ=input("Enter 1 for only residuals, 2 for only model norm, 3 for both: ")

if(plt_typ=='s'):
    clm=1
    if(p_s=="1"):
        xlbl='Smoothing_P_Weight'
    else:
        xlbl='Smoothing_S_Weight'
elif(plt_typ=='d'):
    clm=2;
    if(p_s=="1"):
        xlbl='Damping_P_Weight'
    else:
        xlbl='Damping_S_Weight'
elif(plt_typ=='st'):
    clm=3;
    if(p_s=="1"):
	    xlbl='Station_Correction_P_Weight'
    else:
	    xlbl='Station_Correction_S_Weight'
elif(plt_typ=='hw'):
    clm=4;
    xlbl='Horizontal_Source_Shift_Weight'
elif(plt_typ=='vw'):
    clm=5;
    xlbl='Vertical_Source_Shift_Weight'
elif(plt_typ=='tw'):
    clm=6;
    xlbl='Origin_Time_Shift_Weight'

os.system("rm -f avg_norm_%s_%s_%s.dat"%(itr,1,plt_typ))
os.system("rm -f avg_norm_%s_%s_%s.dat"%(itr,2,plt_typ))
for mdlnm in glob(mdl):
	os.system("cat %s/avg_norm_%s%s.dat >> avg_norm_%s_%s_%s.dat"%(mdlnm,1,itr,itr,1,plt_typ))
	os.system("cat %s/avg_norm_%s%s.dat >> avg_norm_%s_%s_%s.dat"%(mdlnm,2,itr,itr,2,plt_typ))


flnm ="avg_norm_%s_%s_%s.dat"%(itr,1,plt_typ)
dta = np.loadtxt(flnm)
flnm ="avg_norm_%s_%s_%s.dat"%(itr,2,plt_typ)
dta2 = np.loadtxt(flnm)

print(np.shape(dta))
print(np.shape(dta[0]))

if(clm==4 or clm==5 or clm==6 or clm==3):
    if(curv_typ=='3'):
        y_var = (dta[:,8] - np.mean(dta[:,8]))/np.std(dta[:,8]) + \
        (dta[:,7] - np.mean(dta[:,7]))/np.std(dta[:,7]) \
        + (dta2[:,8] - np.mean(dta2[:,8]))/np.std(dta2[:,8]) + \
        (dta2[:,7] - np.mean(dta2[:,7]))/np.std(dta2[:,7])  
    elif(curv_typ=='1'):
        y_var = (dta[:,8] - np.mean(dta[:,8]))/np.std(dta[:,8]) + \
        (dta2[:,8] - np.mean(dta2[:,8]))/np.std(dta2[:,8])
    elif(curv_typ=='2'):
        y_var = (dta[:,7] - np.mean(dta[:,7]))/np.std(dta[:,7]) + \
        (dta2[:,7] - np.mean(dta2[:,7]))/np.std(dta2[:,7])  
    else:
        print("Wrong Argument Value")
        os.system("pause") 
    if (p_s=='1'):
        x_var = dta[:,clm] 
    else:
        x_var = dta2[:,clm] 

else:
    if(curv_typ=='3'):
        y_var = (dta[:,7] - np.mean(dta[:,7]))/np.std(dta[:,7]) + \
        (dta[:,8] - np.mean(dta[:,8]))/np.std(dta[:,8])
    elif(curv_typ=='1'):
        y_var = (dta[:,8] - np.mean(dta[:,8]))/np.std(dta[:,8])
    elif(curv_typ=='2'):
        y_var = (dta[:,7] - np.mean(dta[:,7]))/np.std(dta[:,7])
    else:
        print("Wrong Argument Value")
        os.system("pause") 

if(p_s=='1'):
    x_var = dta[:,clm] 
else:
    x_var = dta2[:,clm] 


idx_xvar = np.argsort(x_var)

x_var = np.sort(x_var)

y_var = y_var[idx_xvar]

if(clm==4):
    vw_var = dta[idx_xvar,5] 
    tw_var = dta[idx_xvar,6] 

if(clm==5):
    hw_var = dta[idx_xvar,4] 
    tw_var = dta[idx_xvar,6] 

if(clm==6):
    vw_var = dta[idx_xvar,5] 
    hw_var = dta[idx_xvar,4] 

if(clm==3 and p_s=='1'):
    s_var = dta2[idx_xvar,3]
    print(s_var)

if(clm==3 and p_s=='2'):
    p_var = dta[idx_xvar,3]

if(clm==2 and p_s=='1'):
    s_var = dta2[idx_xvar,2]
if(clm==2 and p_s=='2'):
    p_var = dta[idx_xvar,2]

if(clm==1 and p_s=='1'):
    s_var = dta2[idx_xvar,1]
if(clm==1 and p_s=='2'):
    p_var = dta[idx_xvar,1]

str_arr=[]
for i in range(0,len(x_var)):
    if(clm==4):
        str_arr.append("v%st%s" % (vw_var[i],tw_var[i]))
    if(clm==5):
        str_arr.append("h%st%s" % (hw_var[i],tw_var[i]))
    if(clm==6):
        str_arr.append("h%sv%s" % (hw_var[i],vw_var[i]))
    if(clm==3 and p_s=='1'):
        str_arr.append("st_s%s" % (s_var[i]))
    if(clm==3 and p_s=='2'):
        str_arr.append("st_p%s" % (p_var[i]))
    if(clm==2 and p_s=='1'):
        str_arr.append("d_s%s" % (s_var[i]))
    if(clm==2 and p_s=='2'):
        str_arr.append("d_p%s" % (p_var[i]))
    if(clm==1 and p_s=='1'):
        str_arr.append("s_s%s" % (s_var[i]))
    if(clm==1 and p_s=='2'):
        str_arr.append("s_p%s" % (p_var[i]))


str_arr = np.array(str_arr)
print(str_arr)

plt.figure(figsize=(30,10),dpi=100)
plt.scatter(x_var,y_var,color='red')
plt.plot(x_var,y_var)
for i in range(0,len(x_var)):
    plt.text(x_var[i]+np.mean(x_var)/10,y_var[i]+np.mean(y_var)/10,str_arr[i])
ax = plt.gca()
#ax.set_aspect('equal', adjustable='box')
plt.minorticks_on()
plt.xlabel(xlbl,fontsize=16,fontweight='bold')
if(curv_typ=='3'):
    plt.ylabel('Normalized Model Norm + RMS residuals',fontsize=16,fontweight='bold')
    pltlbl='residuals+model_norm'
elif(curv_typ=='1'):
    plt.ylabel('Normalized RMS residuals',fontsize=16,fontweight='bold')
    pltlbl='only_residuals'
elif(curv_typ=='2'):
    plt.ylabel('Normalized Model Norm',fontsize=16,fontweight='bold')
    pltlbl='only_model_norm'


if(curv_typ=='3'):
    plt.savefig('./U-curve_{it:d}_{ps:d}_{xlbel:s}_{pltlbel:s}.png'.format(
    it=np.int32(itr),ps=np.int32(p_s),xlbel=xlbl,pltlbel=pltlbl),bbox_inches='tight')
if(curv_typ=='2' or curv_typ=='1'):
    plt.savefig('./L-curve_{it:d}_{ps:d}_{xlbel:s}_{pltlbel:s}.png'.format(
    it=np.int32(itr),ps=np.int32(p_s),xlbel=xlbl,pltlbel=pltlbl),bbox_inches='tight')
plt.show()
