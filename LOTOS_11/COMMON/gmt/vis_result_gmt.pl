#!/usr/bin/perl -w
##############################################
#*******vis_vpvs script***********************
#converts DSAA .grd surfer files to PostScript
#*author*:*Kirill Gadylshin*******************
#*mail*:*gadylshin@yahoo.com******************
#*********************************************
##############################################

use strict;
use warnings;
use lib "../../../COMMON/gmt";
use EnvConfig;
my ($LIST_HOR,$LIST_VER);

my ($ar,$md,$iter) = ("","",);
my $M_PI = 3.1415926535;

my $CONF_FILE_NAME = "../../../DATA/$ar/$md/gmt.conf";

print "$CONF_FILE_NAME \n";

#gmt PARAMS

my ($x_min,$x_max,$y_min,$y_max);
my ($cpt,$cpt_NaN);    
my $ps_out;

my ($PAGE_HEIGHT_HOR, $PAGE_HEIGHT_VER, $PAGE_WIDTH_HOR, $PAGE_WIDTH_VER);

my ($N_ROWS_HOR,$N_COLUMNS_HOR);
my ($N_ROWS_VER,$N_COLUMNS_VER);
my ($DX_HOR,$DY_HOR);
my ($DX_VER,$DY_VER);
my ($N_SEC_VER,$N_SEC_HOR);
my ($XMAP_HOR,$YMAP_HOR,$XMAP_VER,$YMAP_VER);
my ($TICK_X_HOR,$TICK_Y_HOR,$TICK_X_VER,$TICK_Y_VER);
my ($TKMIN_X_VER,$TKMIN_Y_VER);
my $MASK_HOR;

my ($CPT_MAIN,$Z0,$ZN,$N_STEPZ);
my ($MARGIN_LEFT_HOR,$MARGIN_TOP_HOR);
my ($MARGIN_LEFT_VER,$MARGIN_TOP_VER);
my ($TITLE_FONT_SIZE,$TITLE_SHIFT_TOP);
my ($SCALE_THICKNESS_HOR,$SCALE_LENGTH_HOR,$SCALE_SHIFT);
my ($SCALE_THICKNESS_VER,$SCALE_LENGTH_VER);
my (@list_xyz,@list_grd);

my ($MODEL_TYPE);
my ($ANOM_TYPE);

sub usage()
{
    print "vis_result_gmt.pl script takes no less then 6 arguments\n";
    print "\$arg1  -  Area\n";
    print "\$arg2  -  Model\n";
    print "\$arg3  -  Iteration\n";
    print "\$arg4  -  LIST_HOR - filename with list of files for horizontal sections\n";
    print "\$arg5  -  LIST_VER - filename with list of files for vertical sections\n";
    print "\$arg6  -  P, S or VpVs\n";
}

sub configurate()
{

    # Check
    print 'Checking gmt configuration... ';
    
    unless(-f $CONF_FILE_NAME)
    {
	print "FAILED : there is no configuration file\n" ;
	return(1);
    }
    print "OK\n";
    
    # Read
    print "Reading gmt configuration.. [$CONF_FILE_NAME] " ;

    my $res = get_configuration($CONF_FILE_NAME);
    
    unless(defined($res))
    {
	print "Undefined \$res";
	return(1);
    }

#    $LOGLEVEL = get_var("LOGLEVEL");
#    $LOGLEVEL ||= "ALL";
#    log_trace("LOGLEVEL=$LOGLEVEL");
#    set_log_level($LOGLEVEL);
    ($PAGE_WIDTH_HOR,$PAGE_HEIGHT_HOR) = (get_var("PAGE_WIDTH_HOR"),get_var("PAGE_HEIGHT_HOR"));
    ($PAGE_WIDTH_VER,$PAGE_HEIGHT_VER) = (get_var("PAGE_WIDTH_VER"),get_var("PAGE_HEIGHT_VER"));
    #print "PAGE_WIDTH\t= $PAGE_WIDTH\nPAGE_HEIGHT\t= $PAGE_HEIGHT\n";
   
    ($N_ROWS_HOR,$N_COLUMNS_HOR) = (get_var("N_ROWS_HOR"),get_var("N_COLUMNS_HOR"));    
    #print "N_ROWS_HOR\t= $N_ROWS_HOR\nN_COLUMNS_HOR\t= $N_COLUMNS_HOR\n";
    ($N_ROWS_VER,$N_COLUMNS_VER) = (get_var("N_ROWS_VER"),get_var("N_COLUMNS_VER"));
    ($DX_HOR,$DY_HOR) = (get_var("DX_HOR"),get_var("DY_HOR"));
    ($DX_VER,$DY_VER) = (get_var("DX_VER"),get_var("DY_VER"));
    #print "DX\t= $DX\nDY\t= $DY\n";
    ($XMAP_HOR,$YMAP_HOR,$XMAP_VER,$YMAP_VER) = (get_var("XMAP_HOR"),get_var("YMAP_HOR"),get_var("XMAP_VER"),get_var("YMAP_VER"));
    #print "XMAP_HOR\t= $XMAP_HOR\nYMAP_HOR\t= $YMAP_HOR\nXMAP_VER\t= $XMAP_VER\nYMAP_VER\t=$YMAP_VER\n";
    ($N_SEC_HOR,$N_SEC_VER) = (get_var("N_SEC_HOR"),get_var("N_SEC_VER"));
    print "N_SEC_HOR\t= $N_SEC_HOR\nN_SEC_VER\t= $N_SEC_VER\n";
    
    if ($MODEL_TYPE eq "VpVs") # Vp/Vs visualization
    {
	($CPT_MAIN,$Z0,$ZN,$N_STEPZ) =(get_var("CPT_VPVS"),get_var("Z0_VPVS"),get_var("ZN_VPVS"),get_var("N_STEPZ_VPVS"));
    }
    elsif ($ANOM_TYPE eq "anom") # P or S visualization
    {
	($CPT_MAIN,$Z0,$ZN,$N_STEPZ) =(get_var("CPT_ANOM"),get_var("Z0_ANOM"),get_var("ZN_ANOM"),get_var("N_STEPZ"));
    }
    elsif ($MODEL_TYPE eq "P" && $ANOM_TYPE eq "abs")
    {
	($CPT_MAIN,$Z0,$ZN,$N_STEPZ) =(get_var("CPT_ABS"),get_var("Z0_ABS_P"),get_var("ZN_ABS_P"),get_var("N_STEPZ"));
    }
    else
    {
	($CPT_MAIN,$Z0,$ZN,$N_STEPZ) =(get_var("CPT_ABS"),get_var("Z0_ABS_S"),get_var("ZN_ABS_S"),get_var("N_STEPZ"));
    }
    
    ($TICK_X_HOR,$TICK_Y_HOR,$TICK_X_VER,$TICK_Y_VER) = (get_var("TICK_X_HOR"),get_var("TICK_Y_HOR"),get_var("TICK_X_VER"),get_var("TICK_Y_VER"));
    $TKMIN_X_VER = get_var("TKMIN_X_VER");
    $TKMIN_Y_VER = get_var("TKMIN_Y_VER");
    ($MARGIN_LEFT_HOR,$MARGIN_TOP_HOR) = (get_var("MARGIN_LEFT_HOR"),get_var("MARGIN_TOP_HOR"));
    ($MARGIN_LEFT_VER,$MARGIN_TOP_VER) = (get_var("MARGIN_LEFT_VER"),get_var("MARGIN_TOP_VER"));
    
    ($TITLE_FONT_SIZE,$TITLE_SHIFT_TOP) = (get_var("TITLE_FONT_SIZE"),get_var("TITLE_SHIFT_TOP"));
    
    ($SCALE_THICKNESS_HOR,$SCALE_LENGTH_HOR) = (get_var("SCALE_THICKNESS_HOR"),get_var("SCALE_LENGTH_HOR"));
    ($SCALE_THICKNESS_VER,$SCALE_LENGTH_VER) = (get_var("SCALE_THICKNESS_VER"),get_var("SCALE_LENGTH_VER"));
    $SCALE_SHIFT = get_var("SCALE_SHIFT");    
    
    $CPT_MAIN = "../../../COMMON/scales_cpt/$CPT_MAIN";
    
    $MASK_HOR = get_var("MASK_HOR");

    print "OK\n";

    return 0;
}


sub get_cpts($$$$)
{
    my ($cpt_main,$z0,$zn,$n_stepz) = @_;
    my ($cpt,$cpt_NaN);
    $cpt = "../../../DATA/$ar/$md/data/dv_anom.cpt";
    $cpt_NaN = "../../../DATA/$ar/$md/data/dv_anom_NaN.cpt";
    my $dz = ($zn-$z0+0.0)/$n_stepz;    

    my $str_cpt = "";
    
    my $cmd = "gmt makecpt -C$cpt_main -T$z0/$zn > $cpt";
    system($cmd) == 0 or die "Error call makecpt : $?";
    
    open(CPT_IN,"<",$cpt) or die "Can't open $cpt : $!";
    open(CPT_OUT,">",$cpt_NaN) or die "Can't open $cpt_NaN : $!";
    
    my $line;
    my $zn_1 = $zn - 1;
    
    #$line = <CPT_IN>;
    #print $line;
    #print CPT_OUT $line;
    #$line = <CPT_IN>;
    #print CPT_OUT $line;
    #$line = <CPT_IN>;
    #print CPT_OUT $line;
    print CPT_OUT "NaN 125/125/125 NaN 125/125/125\n";
    print CPT_OUT "-10000 125/125/125 -500 125/125/125\n";
    
    if (defined ($line = <CPT_IN>))
    {
        my ($zs,$rs,$zr,$rr) = split(' ',$line);
        #print "$zs,$rs,$zr,$rr";
        print CPT_OUT "-500 $rs $zs $rs\n";
	print CPT_OUT $line;
    }
    else
    {
        die "Error $cpt file format\n";
    }
    
    while (defined ($line = <CPT_IN>))
    {        
        if ($line =~ m/^B.*$/)
        {
            my ($zs,$rs,$zr,$rr) = split(' ',$str_cpt);
            print CPT_OUT "$zn $rs 500 $rs\n";
        }
	print CPT_OUT $line;        
	$str_cpt = $line;
    }
    
    close CPT_IN;
    close CPT_OUT;
    
    $cmd = "gmt makecpt -C$cpt_main -T$z0/$zn > $cpt";
    system($cmd) == 0 or die "Error call makecpt : $?";

    return ($cpt,$cpt_NaN);
}




sub draw_hor_sections()
{      
    return if ($N_ROWS_HOR*$N_COLUMNS_HOR == 0);
    
    print "Converting .grd files to xyz tables...\n";
    open(LIST_HOR,"<",$LIST_HOR) or die "Can't open $LIST_HOR : $!";
    
    my ($grd_in,$xyz_out,$ztr_file);
    my ($title) = "";

    my $n_map = 0;
    
    while (defined($grd_in = <LIST_HOR>))
    {
	$n_map++;
	chomp $grd_in;
	$ztr_file = <LIST_HOR>;
	chomp $ztr_file;
	$title = <LIST_HOR>;
	chomp $title;
	$xyz_out = $grd_in . ".xyz";
	push @list_xyz ,  $xyz_out . '|' . $title . '|' . $ztr_file;   
	convert2xyz($grd_in,$xyz_out);	
    }
    
    
    close LIST_HOR;
    print "Conversion OK\n";
    
    if ($YMAP_HOR == 0)
    {
	$YMAP_HOR = $XMAP_HOR*($y_max-$y_min+0.0)/(($x_max-$x_min+0.0)*cos(($y_max+$y_min+0.0)/2.0*$M_PI/180.0));
    }

    
    die "\$N_ROWS_HOR*\$N_COLUMNS_HOR greater than number of maps\n" if ( $n_map < $N_ROWS_HOR*$N_COLUMNS_HOR );
    
    print "Creating PostScript...\n";
    

    my $cmd = "gmt set MAP_FRAME_TYPE PLAIN MAP_DEFAULT_PEN black FONT_ANNOT_PRIMARY 18p,Helvetica-Bold";
    $cmd = $cmd . " FONT_TITLE 18p,Helvetica-Bold MAP_TITLE_OFFSET -0.3c PS_PAGE_ORIENTATION portrait FONT_LABEL 18p,Helvetica-Bold";    
    system($cmd) == 0 or die "Error call gmtset : $?";
    
    my ($title_x,$title_y) = ($MARGIN_LEFT_HOR,$PAGE_HEIGHT_HOR-$TITLE_SHIFT_TOP);
    
    #if ( $iter != 0 )
    #{
	#$cmd = "echo \"$title_x $title_y $TITLE_FONT_SIZE 0 1 BL Velocity anomalies in horizontal sections. Area : $ar, Model : $md, Iteration : $iter\"";
    #}
    #else
    #{
	#$cmd = "echo \"$title_x $title_y $TITLE_FONT_SIZE 0 1 BL Velocity anomalies in horizontal sections. Area : $ar, Model : $md. Synthetic model\"";
    #}
    
    $cmd = "echo \"\" | gmt pstext -R0/$PAGE_WIDTH_HOR/0/$PAGE_HEIGHT_HOR -JX${PAGE_WIDTH_HOR}c/${PAGE_HEIGHT_HOR}c -Xa0 -Ya0 -K";
    $cmd = $cmd . " --PS_MEDIA=Custom_${PAGE_WIDTH_HOR}cx${PAGE_HEIGHT_HOR}c > $ps_out";
    system($cmd) == 0 or die "Error call pstext : $?";
     


    #Calculating $YMAP
    
    #Draw Grid maps
    for my $i (0..$N_ROWS_HOR-1)
    {
	for my $j (0..$N_COLUMNS_HOR-1)
	{
	    my $info = @list_xyz[$i*$N_COLUMNS_HOR+$j];
	    
	    #print "i = $i j=$j\n";
	    
	    ($xyz_out,$title,$ztr_file) = split(/\|/,$info);
	    if ($i+1 == $N_ROWS_HOR)
	    {
		if ($j == 0)
		{
		    draw_map($xyz_out,$ztr_file,$title,$MARGIN_LEFT_HOR+($XMAP_HOR+$DX_HOR)*$j,$PAGE_HEIGHT_HOR-$MARGIN_TOP_HOR+$DY_HOR-($YMAP_HOR+$DY_HOR)*($i+1),3);
		}
		else
		{
		    draw_map($xyz_out,$ztr_file,$title,$MARGIN_LEFT_HOR+($XMAP_HOR+$DX_HOR)*$j,$PAGE_HEIGHT_HOR-$MARGIN_TOP_HOR+$DY_HOR-($YMAP_HOR+$DY_HOR)*($i+1),2);
		}
	    }
	    elsif($j == 0)
	    {
		draw_map($xyz_out,$ztr_file,$title,$MARGIN_LEFT_HOR+($XMAP_HOR+$DX_HOR)*$j,$PAGE_HEIGHT_HOR-$MARGIN_TOP_HOR+$DY_HOR-($YMAP_HOR+$DY_HOR)*($i+1),1);
	    }
	    else
	    {
		draw_map($xyz_out,$ztr_file,$title,$MARGIN_LEFT_HOR+($XMAP_HOR+$DX_HOR)*$j,$PAGE_HEIGHT_HOR-$MARGIN_TOP_HOR+$DY_HOR-($YMAP_HOR+$DY_HOR)*($i+1),0);
	    }
	}
    }
    
    #Draw scale

    draw_scale_hor();
    return 0;

}

sub draw_scale_hor()
{
#    return if ($SCALE_ORIENTATION eq "");
    
    my ($xpos,$ypos);
#    if ($SCALE_ORIENTATION eq 'h')
#    {
#	$xpos = $MARGIN_LEFT + ($XMAP+$DX)*$N_COLUMNS/2.0 - $DX/2.0;
#	$ypos = $PAGE_HEIGHT-$MARGIN_TOP_HOR+$DY-($YMAP+$DY)*$N_ROWS - $SCALE_SHIFT;
#	my $cmd = "psscale -D${xpos}c/${ypos}c/${SCALE_LENGTH}c/${SCALE_THICKNESS_HOR}ch -S -C$cpt -O -K >> '$ps_out'";    
#	system($cmd) == 0 or die "Error call psscale : $?";
#	$cmd = "echo \"0 0 12 0 1 BC velocity anomalies, %\"";
#	$ypos = $ypos - $SCALE_THICKNESS_HOR - 1.3;
#	$xpos = $xpos - $SCALE_LENGTH_HOR/2.0;
#	$cmd = $cmd . " | pstext -R-10/10/-0.1/10 -JX${SCALE_LENGTH_HOR}c/10c -Xa${xpos} -Ya${ypos} -O >> '$ps_out'";
#	system($cmd) == 0 or die "Error call pstext : $?";	
#    }
#    elsif ($SCALE_ORIENTATION eq 'v')
#    {
	$xpos = $MARGIN_LEFT_HOR + $SCALE_SHIFT;
	$ypos = $PAGE_HEIGHT_HOR-$MARGIN_TOP_HOR-$DY_HOR/2-($YMAP_HOR+$DY_HOR)*$N_ROWS_HOR;
	my $cmd;
    if ($MODEL_TYPE eq "P" || $MODEL_TYPE eq "S")
    {
        $cmd = "gmt psscale -Dx${xpos}c/${ypos}c+w${SCALE_LENGTH_HOR}c/${SCALE_THICKNESS_HOR}ch -B2 -S -C$cpt -O -K >> '$ps_out'";
    }
    else
    {
        $cmd = "gmt psscale -Dx${xpos}c/${ypos}c+w${SCALE_LENGTH_HOR}c/${SCALE_THICKNESS_HOR}ch -B0.06 -S -C$cpt -O -K >> '$ps_out'";
    }
    my $ttl_txt;
    if ($MODEL_TYPE eq "P")
    {
        $ttl_txt="Vp %";
    }
    elsif ($MODEL_TYPE eq "S")
    {
        $ttl_txt="Vs %";
    }
    else
    {
        $ttl_txt=$MODEL_TYPE;
    }    
	system($cmd) == 0 or die "Error call psscale : $?";	
	$cmd = "echo \"0 0 20 0 1 TC ${ttl_txt} \"";
	$xpos = $xpos + $SCALE_LENGTH_HOR/4;
	$ypos = $ypos - $SCALE_THICKNESS_HOR*2.5 - $DY_HOR*4;
	if ($N_ROWS_VER*$N_COLUMNS_VER == 0 )
	{
	    $cmd = $cmd . " | gmt pstext -R-1/1/-2/2 -JX10c/${SCALE_LENGTH_HOR}c -Xa${xpos} -Ya${ypos} -O >> '$ps_out'";
	}
	else
	{
	    $cmd = $cmd . " | gmt pstext -R-1/1/-2/2 -JX10c/${SCALE_LENGTH_HOR}c -Xa${xpos} -Ya${ypos} -O >> '$ps_out'";
	}
	system($cmd) == 0 or die "Error call pstext : $?";
#    }
}

sub draw_vert_sections($$$)
{
    my ($mdl_typ,$iter,$ar) = @_;
    return if ($N_ROWS_VER*$N_COLUMNS_VER == 0);
    my $SHIFT_TOP;
    if ($N_ROWS_HOR*$N_COLUMNS_HOR == 0 )
    {
	$SHIFT_TOP = $PAGE_HEIGHT_VER;
    }
    else
    {
	$SHIFT_TOP = $PAGE_HEIGHT_VER-$MARGIN_TOP_VER+$DY_VER - $SCALE_SHIFT;
    }
    
    my ($title_x,$title_y) = ($MARGIN_LEFT_VER,$SHIFT_TOP - $TITLE_SHIFT_TOP);
    
    my $cmd;

    #if ( $iter != 0 )
    #{
	#$cmd = "echo \"$title_x $title_y $TITLE_FONT_SIZE 0 1 BL Velocity anomalies in vertical sections. Area : $ar, Model : $md, Iteration : $iter\"";
    #}
    #else
    #{
	#$cmd = "echo \"$title_x $title_y $TITLE_FONT_SIZE 0 1 BL Velocity anomalies in vertical sections. Area : $ar, Model : $md. Synthetic model\"";
    #}
    
    if ($N_ROWS_HOR*$N_COLUMNS_HOR == 0 )
    {
	$cmd = "echo \"\" | gmt pstext -R0/$PAGE_WIDTH_VER/0/$PAGE_HEIGHT_VER -JX${PAGE_WIDTH_VER}c/${PAGE_HEIGHT_VER}c -Xa0 -Ya0 -K";
	$cmd = $cmd . " --PAPER_MEDIA=Custom_${PAGE_WIDTH_VER}cx${PAGE_HEIGHT_VER}c > $ps_out";
    }
    else
    {
	$cmd = "echo \"\" | gmt pstext -R0/$PAGE_WIDTH_VER/0/$PAGE_HEIGHT_VER -JX${PAGE_WIDTH_VER}c/${PAGE_HEIGHT_VER}c -Xa0 -Ya0 -K";
	$cmd = $cmd . " --PAPER_MEDIA=Custom_${PAGE_WIDTH_VER}cx${PAGE_HEIGHT_VER}c > $ps_out";
    }
    #$cmd = "gmt set PS_MEDIA=Custom_${PAGE_WIDTH_VER}cx${PAGE_HEIGHT_VER}c;";
    system($cmd) == 0 or die "Error call pstext : $?";
 
    #($cpt,$cpt_NaN) = get_cpts($CPT_MAIN,$Z0,$ZN,$N_STEPZ);
    my ($grd_in,$xyz_out,$ztr_file,$zlg_file);
    my ($title) = "";

    my $n_map = 0;
    
    open(LIST_VER,"<",$LIST_VER) or die "Can't open $LIST_VER : $!";
    
    while (defined($grd_in = <LIST_VER>))
    {
	$n_map++;
	chomp $grd_in;
	$ztr_file = <LIST_VER>;
	chomp $ztr_file;
	$zlg_file = <LIST_VER>;
	chomp $zlg_file;
	$title = <LIST_VER>;
	chomp $title;
	#$xyz_out = $grd_in . ".xyz";
	push @list_grd ,  $grd_in . '|' . $title . '|' . $ztr_file . '|' . $zlg_file;   
	#convert2xyz($grd_in,$xyz_out);	
    }
    
    $n_map = 0;
    close LIST_VER;

    my ($n_ver,$lonlw,$latlw,$lonhg,$lathg,$line);

    my $flnm_setver;

    $flnm_setver = "../../../DATA/";
    $flnm_setver = $flnm_setver . "$ar";
    $flnm_setver = $flnm_setver . "/setver.dat";

    open(VER,"<",$flnm_setver) or die "Can't open '$grd_in' : $!";
    die "Error: Incorrect file format" unless (defined ($line = <VER>) );
    ($n_ver) = split(' ',$line);

    $cmd = "gmt blockmean ../../../Moho_Sodoudi.dat -R18.7/30.7/32.5/43 -I5m -i1,2,4 > test.xyg;";
    $cmd = $cmd . "gmt grdmask ../../../Greece_poly_coord.txt -R18.7/30.7/32.5/43 -I0.01=/0.01= -NNaN/1/1 -Gnew.nc -V;";
    $cmd = $cmd . "gmt surface test.xyg -R18.7/30.7/32.5/43 -I0.05=/0.05= -Gmoho_sodoudi.nc -T0.25 -V;";
    #$cmd = $cmd . "gmt grdmath moho_sodoudi.nc new.nc OR = moho_trimmed.grd;";
    $cmd = $cmd . "gmt grdconvert moho_sodoudi.nc -Gmoho_trimmed.grd -V;";
    #system($cmd) == 0 or die "Error call pstext : $?";

    $cmd = "gmt blockmean ../../../Moho_African_Sodoudi.dat -R18.7/30.7/32.5/43 -I5m -i1,2,4 > test.xyg;";
    $cmd = $cmd . "gmt grdmask ../../../Greece_poly_coord.txt -R18.7/30.7/32.5/43 -I0.01=/0.01= -NNaN/1/1 -Gnew.nc -V;";
    $cmd = $cmd . "gmt surface test.xyg -R18.7/30.7/32.5/43 -I0.05=/0.05= -Gmoho_sodoudi.nc -T0.25 -V;";
    #$cmd = $cmd . "gmt grdmath moho_sodoudi.nc new.nc OR = moho_trimmed.grd;";
    $cmd = $cmd . "gmt grdconvert moho_sodoudi.nc -Gmoho_trimmed_African.grd -V;";
    #system($cmd) == 0 or die "Error call pstext : $?";

    #Calculating MAPs sizes
    #calc_map_sizes(0);
    
    #die "\$N_ROWS_VER*\$N_COLUMNS_VER greater than number of maps\n" if ( $n_map < $N_ROWS_VER*$N_COLUMNS_VER );
   
    print "OK\n";
    #Draw Vertical sections

    my ($OFF_X,$OFF_Y,$Y_HGH,$lbl_xpos,$lbl_ypos);
    
    
    my ($SHIFT_LEFT);
    my $XMAP_CUR;
    my $XMAP_ROW=0;
    my $k=0;

    my ($latlw_dom,$lathg_dom,$lonlw_dom,$lonhg_dom); 

    $latlw_dom=9999;
    $lathg_dom=-9999;
    $lonlw_dom=9999;
    $lonhg_dom=-9999;
    
    for my $i (0..$N_ROWS_VER-1)
    {
	$SHIFT_LEFT = 0;
  	$XMAP_CUR=$MARGIN_LEFT_VER;
	for my $j (0..$N_COLUMNS_VER-1)
	{
    	$k = $k + 1;
        if ($k <= $N_SEC_VER)
        {
            #print "$i*$j \n";
            #print "$N_SEC_VER \n";
	        my $info = @list_grd[$i*$N_COLUMNS_VER+$j];
	    
	        #print "i = $i j=$j\n";
	    
	        ($grd_in,$title,$ztr_file,$zlg_file) = split(/\|/,$info);
	        $xyz_out = $grd_in . ".xyz";	
	        convert2xyz($grd_in,$xyz_out);
	        if ($XMAP_VER == 0)
	        {
		        $XMAP_VER = ($x_max-$x_min)/($y_max-$y_min)*$YMAP_VER;
	        }
	    
          $OFF_X = $MARGIN_LEFT_VER + $SHIFT_LEFT;
          $OFF_Y = $SHIFT_TOP - ($YMAP_VER+$DY_VER)*($i+1) - $DY_VER;

          $n_map = $n_map + 1;

          $lbl_xpos = $x_max - 1;
          $lbl_ypos = $y_max - 1;

          die "Error: Incorrect file format" unless (defined ($line = <VER>) );
          ($lonlw,$latlw,$lonhg,$lathg) = split(' ',$line);

	  if ($lonlw < $lonlw_dom)
	  {
		  $lonlw_dom = $lonlw;
	  }

	  if ($lonhg > $lonhg_dom)
	  {
		  $lonhg_dom = $lonhg;
	  }

	  if ($latlw < $latlw_dom)
	  {
		  $latlw_dom = $latlw;
	  }

	  if ($lathg > $lathg_dom)
	  {
		  $lathg_dom = $lathg;
	  }

	  draw_map_ver($xyz_out,$ztr_file,$zlg_file,$title,$OFF_X,$OFF_Y,$lonlw,$latlw,$lonhg,$lathg,$x_min,$x_max,$lbl_xpos,$lbl_ypos,$n_map,$mdl_typ,$iter);

          $cmd = "gmt project -C$lonlw/$latlw -E$lonhg/$lathg -G0.01 > temp.xy;";
          $cmd = $cmd . "gmt grdtrack temp.xy -Gmoho_trimmed.grd -i0:1 > temp.xyz;";
          $cmd = $cmd . "gmt mapproject temp.xy -R18.7/30.7/32.5/43 -E -G$lonlw/$latlw+uk -i0:1 > temp.txt;";
          $cmd = $cmd . "paste temp.txt temp.xyz | awk \'\{ if(\$6>0) print \$3,-\$6\}\' > temp.z;";
          $cmd = $cmd . "gmt psxy temp.z -R$x_min/$x_max/$y_min/$y_max -JX${XMAP_VER}c/${YMAP_VER} -W8p,0/100/0 -Xa${OFF_X}c -Ya${OFF_Y}c -O -K -N >> '$ps_out';";
          #84/22/180
          #$cmd = $cmd . "head -n5 temp.z;";
          #system($cmd) == 0 or die "Error call pscoast : $?";

          $cmd = "gmt project -C$lonlw/$latlw -E$lonhg/$lathg -G0.01 > temp.xy;";
          $cmd = $cmd . "gmt grdtrack temp.xy -Gmoho_trimmed_African.grd -i0:1 > temp.xyz;";
          $cmd = $cmd . "gmt mapproject temp.xy -R18.7/30.7/32.5/43 -E -G$lonlw/$latlw+uk -i0:1 > temp.txt;";
          $cmd = $cmd . "paste temp.txt temp.xyz | awk \'\{ if(\$6>0) print \$3,-\$6\}\' > temp.z;";
          $cmd = $cmd . "gmt psxy temp.z -R$x_min/$x_max/$y_min/$y_max -JX${XMAP_VER}c/${YMAP_VER} -W8p,255/0/0 -Xa${OFF_X}c -Ya${OFF_Y}c -O -K -N >> '$ps_out';";
          #$cmd = $cmd . "head -n5 temp.z;";
          #system($cmd) == 0 or die "Error call pscoast : $?";

	  $SHIFT_LEFT = $SHIFT_LEFT + $XMAP_VER + $DX_VER;
          $XMAP_CUR = $XMAP_CUR + $XMAP_VER + $DX_VER;
          $XMAP_VER = get_var("XMAP_VER");
          #print "$XMAP_VER \n"
        }
	}
          if ($XMAP_CUR > $XMAP_ROW)
          {
	          $XMAP_ROW = $XMAP_CUR;
          } 
    }

    close VER;

    if ($N_ROWS_VER == $N_COLUMNS_VER)
    {
        $OFF_Y = $SHIFT_TOP-($YMAP_VER+$DY_VER)*($N_ROWS_VER) + $DY_VER;
        $OFF_X = $MARGIN_LEFT_VER + $XMAP_ROW - $DX_VER;
    }
    else
    {
        $OFF_Y = $SHIFT_TOP-($YMAP_VER+$DY_VER)*($N_ROWS_VER)+ $DY_VER;
        $OFF_X = $MARGIN_LEFT_VER + $XMAP_ROW - $DX_VER;
    }

    $lonlw_dom = $lonlw_dom - 1;
    $latlw_dom = $latlw_dom - 1;
    $lonhg_dom = $lonhg_dom + 1;
    $lathg_dom = $lathg_dom + 1;

    $cmd = "gmt set FONT_LABEL = 28p,Helvetica-Bold,black; gmt set MAP_SCALE_HEIGHT=15p; gmt makecpt -Cgray -T-8000/0/100 -Z > custom.cpt;";
    $cmd = $cmd . "gmt set FONT_ANNOT_PRIMARY = 28p,Helvetica-Bold,black;";
    $cmd = $cmd . "gmt psbasemap -Rg$lonlw_dom/$lonhg_dom/$latlw_dom/$lathg_dom -Jm6.0c -Bxa1.0f0.2+l'Longitude' -Bya1.0f0.2+l'Latitude' -BWeSn -O -K -Xa${OFF_X}c -Ya${OFF_Y}c >> '$ps_out';";
    $cmd = $cmd . "gmt grdimage ../../../COMMON/gmt/ETOPO1_Bed_g_gmt4.grd -Itopo.int -Ctopo_hillshade.cpt -R -J -BWeSn -Xa${OFF_X}c -Ya${OFF_Y}c -O -V -K >> '$ps_out';";
    $cmd = $cmd . "gmt pscoast -R -J -Dh -N1/0.5,100/0/0 -I1/0.5,blue -W0.5 -Lg20.5/35+c35+w100k+at+f+l'Km'";
    
    $cmd = $cmd . " -Xa${OFF_X}c -Ya${OFF_Y}c -O -K >> '$ps_out'";	
    system($cmd) == 0 or die "Error call pscoast : $?";


    
    open(IN,"<","../../../DATA/$ar/setver.dat") or die "Can't open '$grd_in' : $!";
    die "Error: Incorrect file format" unless (defined ($line = <IN>) );
    ($n_ver) = split(' ',$line);
    my ($plt_idx);
    for my $i (0..$n_ver-1)
    {
        #print "$i \n";
        $plt_idx = $i + 1;
        die "Error: Incorrect file format" unless (defined ($line = <IN>) );
        ($lonlw,$latlw,$lonhg,$lathg) = split(' ',$line);
        #print "$lonlw $latlw $lonhg $lathg \n";
        $cmd = "echo '$lonlw $latlw' > test.txt;";
        $cmd = $cmd . " echo '$lonhg $lathg' >> test.txt;";
        print "$cmd \n";
        system($cmd) == 0 or die "Error call pscoast : $?";
        $cmd = "gmt psxy test.txt -Rg$lonlw_dom/$lonhg_dom/$latlw_dom/$lathg_dom -J -W4p,red -SqD100k:+l\"|\"+f32p,Helvetica-Bold,red";
        $cmd = $cmd . " -Xa${OFF_X}c -Ya${OFF_Y}c -O -K >> '$ps_out';";
        #print "$cmd \n";
        #system($cmd) == 0 or die "Error call pscoast : $?";
        #print "$lonlw $latlw $plt_idx";
        $cmd = $cmd . "gmt psxy test.txt -R -J -Ss40p -Gwhite -t50";
        $cmd = $cmd . " -Xa${OFF_X}c -Ya${OFF_Y}c -O -K >> '$ps_out';";

        $cmd = $cmd . "echo \"$lonlw $latlw $plt_idx\" | gmt pstext -R -J -F+f32p,Helvetica-Bold,blue";
        $cmd = $cmd . " -Xa${OFF_X}c -Ya${OFF_Y}c -O -K >> '$ps_out';";
        #print "$cmd \n";
        #system($cmd) == 0 or die "Error call pscoast : $?";

        $cmd = $cmd . "echo \"$lonhg $lathg $plt_idx\'\" | gmt pstext -R -J -F+f32p,Helvetica-Bold,blue";
        $cmd =$cmd . " -Xa${OFF_X}c -Ya${OFF_Y}c -O -K >> '$ps_out';";
        print "$cmd \n";
        system($cmd) == 0 or die "Error call pscoast : $?";
    }

    close IN;

    print "$mdl_typ \n";
    draw_scale_vert($mdl_typ,$XMAP_ROW);
    
    return 0;
}

sub main()
{
    if ($#ARGV + 1 < 6 )
    {
        usage();
        exit;
    }
        
    $ar = $ARGV[0];
    $md = $ARGV[1];
    $iter = $ARGV[2];
    $LIST_HOR = $ARGV[3];
    $LIST_VER = $ARGV[4];
    $MODEL_TYPE = $ARGV[5];
    $ANOM_TYPE = $ARGV[6];
    
    $CONF_FILE_NAME = "../../../DATA/$ar/$md/gmt.conf";
    die "Configuration process has been FAILED\n" if (configurate() != 0 );

    
    print "Creating PostScript...\n";
    
    $ENV{'PATH'} = $ENV{'PATH'} . ':/usr/bin/gmt';

    my $cmd;
    ($cpt,$cpt_NaN) = get_cpts($CPT_MAIN,$Z0,$ZN,$N_STEPZ);  

    #if ($ANOM_TYPE eq "anom" || $MODEL_TYPE eq "VpVs")
    #{
      $ps_out = "../../../PICS/ps/$ar/$md/vis_result_hor_${ANOM_TYPE}_${MODEL_TYPE}_${iter}.ps";  
  
      #Draw Horizontal sections
      print "Draw Horizontal sections...\n";
    
      draw_hor_sections();

      $cmd="gmt psconvert $ps_out -Tf; rm -f $ps_out;";
      system($cmd);
      #}

    #Draw Vertical sections
    print "Draw Vertical sections...\n";

    $ps_out = "../../../PICS/ps/$ar/$md/vis_result_ver_${ANOM_TYPE}_${MODEL_TYPE}_${iter}.ps";
    
    #print "$MODEL_TYPE \n";
    draw_vert_sections($MODEL_TYPE,$iter,$ar);

    $cmd="gmt psconvert $ps_out -Tf; rm -f $ps_out;";
    system($cmd);
    
    return 0;
}

#draw plot marks
sub draw_marks($$$$)
{
    my ($mark_dat,$mark_bln,$OFF_X,$OFF_Y) = @_;
    
    #Plot marks vor vertical sections
#    open(MARK,"<",$mark_dat) or die "Can't open $mark_dat : $!";
    
    my ($line,$cmd);
#    my ($x,$y,$mark_text);
#    while (defined ($line = <MARK>))
#    {	
#	($x,$y,$mark_text) = split(' ',$line);
#	$mark_text = sprintf("%3.0f",$mark_text);
#	$cmd = "echo \"$x $y $TITLE_FONT_SIZE 0 1 BC $mark_text\"";
#	$cmd = $cmd . " | pstext -R$x_min/$x_max/$y_min/$y_max -JX${XMAP_HOR}c/${YMAP_HOR}c";
#	$cmd = $cmd . " -Xa${OFF_X}c -Ya${OFF_Y}c -O -K >> '$ps_out'";
#	system($cmd) == 0 or die "Error call pstext : $?";
 #   }
#    close(MARK);
    
#    $cmd="psxy '$mark_dat' -Rg$x_min/$x_max/$y_min/$y_max -JX${XMAP_HOR}c/${YMAP_HOR}c -S0.13c -fig ";
    
#    $cmd = $cmd . " -Xa${OFF_X}c -Ya${OFF_Y}c -O -K >> '$ps_out'";
#    print $cmd . "\n";
#    system($cmd) == 0 or die "Error call psxy : $?";
    
    
    $cmd="gmt psxy '$mark_bln' -Rg$x_min/$x_max/$y_min/$y_max -JX${XMAP_HOR}c/${YMAP_HOR}c -SqD50k:+LDk -H -W0.05c";
    
    $cmd = $cmd . " -Xa${OFF_X}c -Ya${OFF_Y}c -O >> '$ps_out'";
    print $cmd . "\n";
    #system($cmd) == 0 or die "Error call psxy : $?";
}


#draw plot map
sub draw_map($$$$$$)
{
    #$axis  0 - no axis labels
    #$axis  1 - y-axis label
    #$axis  2 - x-axis label
    #$axis 3 - x- and y-axis labels    
    my ($xyz_out,$ztr,$title,$OFF_X,$OFF_Y,$axis) = @_;
    
    my $cmd;

    if ($MASK_HOR eq "True" || $MASK_HOR eq "")
    {
        $cmd="gmt blockmean '$xyz_out' -R$x_min/$x_max/$y_min/$y_max -I1m > test.xyg;";
        $cmd = $cmd . " gmt grdmask ../../../Greece_poly_coord.txt -R$x_min/$x_max/$y_min/$y_max -I0.01=/0.01= -NNaN/1/1 -Gnew.nc -V;";
        $cmd = $cmd . " gmt surface test.xyg -R$x_min/$x_max/$y_min/$y_max -I0.01=/0.01= -GSAegean_grd24.nc -T1.0 -V;";
        $cmd = $cmd . " gmt grdmath SAegean_grd24.nc new.nc OR = output24.grd;";
        $cmd = $cmd . " gmt grdimage output24.grd -R$x_min/30/33/42 -Jm1.0c -C$cpt_NaN";
    } else
    {
        $cmd="gmt blockmean '$xyz_out' -R$x_min/$x_max/$y_min/$y_max -I1m > test.xyg;";
        $cmd = $cmd . " gmt surface test.xyg -R$x_min/$x_max/$y_min/$y_max -I0.01=/0.01= -GSAegean_grd24.nc -T1.0 -V;";
        $cmd = $cmd . " gmt grdimage SAegean_grd24.nc -R$x_min/$x_max/$y_min/$y_max -Jm8.0c -C$cpt_NaN";
    }
    $cmd = $cmd . " -Bxa${TICK_X_HOR}g${TICK_X_HOR} -Bya${TICK_X_HOR}g${TICK_X_HOR} -BWeSn+t\"$title\"";
    
    $cmd = $cmd . " -Xa${OFF_X}c -Ya${OFF_Y}c -O -K >> '$ps_out'";
    print "$cmd\n";
    system($cmd) == 0 or die "Error call pscontour : $?";
    
    
    $cmd="gmt pscoast -Rg$x_min/$x_max/$y_min/$y_max -Jm8c -Dh -N1/0.5,100/0/0 -I1/0.5,blue -W0.5";
    
    $cmd = $cmd . " -Xa${OFF_X}c -Ya${OFF_Y}c -O -K >> '$ps_out'";	
    system($cmd) == 0 or die "Error call pscoast : $?";
   
    $cmd="gmt psxy '$ztr' -Rg$x_min/$x_max/$y_min/$y_max -JX${XMAP_HOR}c/${YMAP_HOR}c -Gblack -Sc0.05c";
    #"  psxy circle.dat -Rg-180/-120/50/70 -Jx0.16id -Gred -B4/4 -fig -Sc0.03c 
    $cmd = $cmd . " -Xa${OFF_X}c -Ya${OFF_Y}c -O -K >> '$ps_out'";
    

    #system($cmd) == 0 or die "Error call psxy : $?";

}

sub draw_map_ver($$$$$$$$$$$$$$$$)
{
    my ($xyz_out,$ztr,$zlg,$title,$OFF_X,$OFF_Y,$lonlw,$latlw,$lonhg,$lathg,$x_min,$x_max,$lbl_xpos,$lbl_ypos,$n_map,$mdl_typ,$iter) = @_;
    
    my ($cmd,$chk_fl,$chk_tmp,$chk_syn);

    $cmd = "gmt project -C$lonlw/$latlw -E$lonhg/$lathg -G0.01 > temp.xy;";
    $cmd = $cmd . "gmt grd2xyz -R$lonlw/$lonhg/$latlw/$lathg ../../../COMMON/gmt/ETOPO1_Bed_g_gmt4.grd > temp1.xyz;";
    $cmd = $cmd . "awk \'{ print \$1,\$2,\$3/1000 }\' temp1.xyz > ../../../COMMON/gmt/topo.xyz;";
    $cmd = $cmd . "gmt grdtrack temp.xy -G../../../COMMON/gmt/topo.xyz -i0:1 > temp.xyz;";
    $cmd = $cmd . "gmt mapproject temp.xy -R$lonlw/$lonhg/$latlw/$lathg -E -G$lonlw/$latlw+uk -i0:1 > temp.txt;";
    $cmd = $cmd . "paste temp.txt temp.xyz | awk \'{ print \$3,\$6}\' > temp.z;";
    print "$cmd\n";
    system($cmd) == 0 or die "Error call project, grd2xyz, grdtrack, mapproject : $?";

    $cmd ="gmt set MAP_TITLE_OFFSET=10p; gmt set MAP_TICK_PEN_PRIMARY=thicker,black;";
    $cmd = $cmd . "gmt set FONT_LABEL=28p,Helvetica-Bold; gmt set FONT_ANNOT_PRIMARY=28p,Helvetica-Bold,black;";
    $cmd = $cmd . "gmt set MAP_GRID_PEN_PRIMARY=thinnest,black,dashed;";
    $cmd = $cmd . "gmt set MAP_TICK_PEN_PRIMARY=thin,black,solid;";
    # $cmd = $cmd . "gmt blockmean '$xyz_out' -R$x_min/$x_max/$y_min/$y_max -I1 -V > test.xyg;";
    # $cmd = $cmd . " gmt surface test.xyg -R$x_min/$x_max/$y_min/$y_max -I1=/1= -GSAegean_grd24.nc -T1.0 -V;";
    # $cmd = $cmd . " gmt grdimage SAegean_grd24.nc -R$x_min/$x_max/$y_min/$y_max -JX${XMAP_VER}c/${YMAP_VER}c -C$cpt_NaN";

    
    $cmd= $cmd . "gmt pscontour -R$x_min/$x_max/$y_min/$y_max -JX${XMAP_VER}c/${YMAP_VER}c '$xyz_out' -C$cpt_NaN -I -V";
    $cmd = $cmd . " -Ba${TICK_X_VER}f${TKMIN_X_VER}g${TICK_X_VER}:\"distance (km)\":/a${TICK_Y_VER}f${TKMIN_Y_VER}g${TICK_Y_VER}:\"depth (km)\"::.:WeSn";
    $cmd = $cmd . " -Xa${OFF_X}c -Ya${OFF_Y}c -O -K >> '$ps_out';";
    $cmd = $cmd . " gmt psxy temp.z -R -J -B -W3p,0/0/0 -Xa${OFF_X}c -Ya${OFF_Y}c -O -K >> '$ps_out';";
    $cmd = $cmd . "echo 1 $lbl_ypos $n_map | gmt pstext -R -J -B -F+f44p,Helvetica-Bold,blue -Ggrey -Xa${OFF_X}c -Ya${OFF_Y}c -O -K -V >> '$ps_out';";
    $cmd = $cmd . "echo \"$lbl_xpos $lbl_ypos $n_map\'\" | gmt pstext -R -J -B -F+f44p,Helvetica-Bold,blue -Ggrey -Xa${OFF_X}c -Ya${OFF_Y}c -O -K -V >> '$ps_out';";
    print "$cmd\n";
    system($cmd) == 0 or die "Error call pscontour : $?";
    

=begin comment
    if ($mdl_typ eq 'VpVs')
    {
      $chk_fl = $xyz_out =~ s/_mdl_/_chk_/r;
      $chk_syn = $xyz_out =~ s/ver_1[1-9]/syn_vpvs/r;
      #print "$chk_syn\n";
      $cmd="paste '$chk_syn' '${chk_fl}' > chk.txt; awk \'{if(\$3==NaN || \$3*\$6<0) print \$1,\$2,\$3*\$6}\' chk.txt > temp2.txt; gmt psmask temp2.txt -R -J -I5 -T -Ggray -V";
      $cmd = $cmd . " -Xa${OFF_X}c -Ya${OFF_Y}c -O -K >> '$ps_out';";
      print "$cmd\n";
      system($cmd) == 0 or die "Error call pscontour : $?";
    }
    elsif ($mdl_typ eq 'P')
    {
      $chk_fl = $xyz_out =~ s/ver_/chk_/r;
      $chk_tmp = $xyz_out =~ s/ver_1[1-9]/syn_dv/r;
      $chk_syn = $chk_tmp =~ s/\.grd/1\.grd/r;
      #print "$chk_tmp $chk_syn\n";
      $cmd="paste '$chk_fl' '${chk_syn}' > chk.txt; awk \'{if(\$3==NaN || \$3*\$6<0) print \$1,\$2,\$3*\$6}\' chk.txt > temp2.txt; gmt psmask temp2.txt -R -J -I5 -T -Ggray -V";
      $cmd = $cmd . " -Xa${OFF_X}c -Ya${OFF_Y}c -O -K >> '$ps_out';";
      print "$cmd\n";
      system($cmd) == 0 or die "Error call pscontour : $?";
    }
    else
    {
      $chk_fl = $xyz_out =~ s/ver_/chk_/r;
      $chk_tmp = $xyz_out =~ s/ver_2[1-9]/syn_dv/r;
      $chk_syn = $chk_tmp =~ s/\.grd/2\.grd/r;
      #print "$chk_tmp $chk_syn\n";
      $cmd="paste '$chk_fl' '${chk_syn}' > chk.txt; awk \'{if(\$3==NaN || \$3*\$6<0) print \$1,\$2,\$3*\$6}\' chk.txt > temp2.txt; gmt psmask temp2.txt -R -J -I5 -T -Ggray -V";
      $cmd = $cmd . " -Xa${OFF_X}c -Ya${OFF_Y}c -O -K >> '$ps_out';";
      print "$cmd\n";
      system($cmd) == 0 or die "Error call pscontour : $?";
    }
=end comment 

=cut
    
    #$cmd="pscoast -Rg$x_min/$x_max/$y_min/$y_max -JX${XMAP_VER}c/${YMAP_VER}c -Di -N1/3,100/0/0 -I1/3,blue -W3";
    
    #$cmd = $cmd . " -Xa${OFF_X}c -Ya${OFF_Y}c -O -K >> '$ps_out'";	
    #system($cmd) == 0 or die "Error call pscoast : $?";
   
    $cmd="gmt psxy '$ztr' -R$x_min/$x_max/$y_min/$y_max -JX${XMAP_VER}c/${YMAP_VER}c -Ggreen -Sc0.10c -V";
    $cmd = $cmd . " -Xa${OFF_X}c -Ya${OFF_Y}c -O -K >> '$ps_out'";
    
    system($cmd) == 0 or die "Error call psxy : $?";

    $cmd="gmt pscoupe '$zlg' -R$x_min/$x_max/$y_min/$y_max -JX${XMAP_VER}c/${YMAP_VER}c -Aa$lonlw/$latlw/$lonhg/$lathg/90/20/0/50 -Ggreen -W0.4p,black -Sa0.8 -V";
    $cmd = $cmd . " -Xa${OFF_X}c -Ya${OFF_Y}c -O -K >> '$ps_out'";
    #print "$cmd\n";
    
    #system($cmd) == 0 or die "Error call pscoupe : $?";
}


sub draw_scale_vert($)
{
    my ($mdl_typ,$XMAP_ROW) = @_;
    my ($xpos,$ypos,$scale_abs_lnth);
    $xpos = $MARGIN_LEFT_VER+$XMAP_ROW-$DX_VER;
    
	  $ypos = $PAGE_HEIGHT_VER - ($YMAP_VER+$DY_VER)*($N_ROWS_VER);
    
    $scale_abs_lnth = $SCALE_LENGTH_VER * 1.2;
    
    #$ypos = $ypos - $MARGIN_TOP_VER+$DY-($YMAP_VER+$DY)*($N_ROWS_VER) - $SCALE_SHIFT;
    my ($cmd);
    #print "$mdl_typ \n";
    if ($mdl_typ eq "VpVs")
    {
        $cmd = "gmt psscale -Dx${xpos}c/${ypos}c+w${SCALE_LENGTH_VER}c/${SCALE_THICKNESS_VER}ch -B0.06 -S -C$cpt -O -K >> '$ps_out'";
    }
    elsif ($ANOM_TYPE eq "abs")
    {
        $cmd = "gmt psscale -Dx${xpos}c/${ypos}c+w${scale_abs_lnth}c/${SCALE_THICKNESS_VER}ch -B0.2 -S -C$cpt -O -K >> '$ps_out'";
    }
    else 
    {
        $cmd = "gmt psscale -Dx${xpos}c/${ypos}c+w${SCALE_LENGTH_VER}c/${SCALE_THICKNESS_VER}ch -B2 -S -C$cpt -O -K >> '$ps_out'";
    }
    system($cmd) == 0 or die "Error call psscale : $?";
    my $ttl_txt;
    if ($mdl_typ eq "P")
    {
        $ttl_txt="Vp %";
    }
    elsif ($mdl_typ eq "S")
    {
        $ttl_txt="Vs %";
    }
    else
    {
        $ttl_txt="Vp/Vs";
    }
    $cmd = "echo \"0 0 40 0 1 BC $ttl_txt \"";
    $ypos = $ypos - $SCALE_THICKNESS_VER - 2.7;
    $xpos = $xpos;
    $cmd = $cmd . " | gmt pstext -R-10/10/-0.4/10 -JX${SCALE_LENGTH_VER}c/10c -Xa${xpos} -Ya${ypos} -O >> '$ps_out'";
    system($cmd) == 0 or die "Error call pstext : $?";	
}

#convert DSAA surfer .grd file into xyz table
sub convert2xyz($$)
{
    my ($grd_in,$xyz_out) = @_;
    
    open(IN,"<",$grd_in) or die "Can't open '$grd_in' : $!";
    open(OUT,'>',$xyz_out) or die "Can't open $xyz_out : $!";
    
    print "Start converting $grd_in to xyz table...\n";
    
    my ($header,$nx,$ny,$z_min,$z_max,$line);
    

    die "Error: Incorrect file format" unless (defined ($header = <IN>) );
    
    die "Error: Incorrect file format" unless (defined ($line = <IN>) );
    ($nx,$ny) = split(' ',$line);

    die "Error: Incorrect file format" unless (defined ($line = <IN>) );
    ($x_min,$x_max) = split(' ',$line);
    
    die "Error: Incorrect file format" unless (defined ($line = <IN>) );
    ($y_min,$y_max) = split(' ',$line);
    
    die "Error: Incorrect file format" unless (defined ($line = <IN>) );
    ($z_min,$z_max) = split(' ',$line);
    
    die "Error: Incorrect file format" if ($ny < 2 || $nx < 2);
    

    my ($recn,$cnt) = ($nx*$ny,0);

    my ($dx,$dy) = (($x_max-$x_min+0.0)/($nx-1.0),($y_max-$y_min+0.0)/($ny-1.0));		
    
    while ($cnt < $recn && defined($line = <IN>))
    {      
 	for my $z (split(' ',$line)) 
	{
            $cnt++;
            my ($x,$y) = ($x_min+($cnt%$nx)*$dx,$y_min+($cnt/$nx)*$dy);
            if ($z eq "-999.000000")
            {
                $z = "NaN"
            }
	    print OUT "$x $y $z\n";            
	}
    }

    die "Failed to convert. Not enough data in $grd_in. Terminated." if ( $cnt < $recn);
    warn "Too many data in $grd_in. Cutted." if (<IN>);
    
    
    close OUT;
    close IN;

    print "Converting OK. Processed $cnt grid nodes\n";
    
}

sub calc_map_sizes($)
{
    my $i = shift;
    if ($YMAP_HOR == 0)
    {
        my $grd_in = "../../../TMP_files/hor/dv11 1.grd";
	open(IN,"<",$grd_in) or die "Can't open '$grd_in' : $!";
	my $line;
        die "Error: Incorrect file format" unless (defined ($line = <IN>) );    
        die "Error: Incorrect file format" unless (defined ($line = <IN>) );
        die "Error: Incorrect file format" unless (defined ($line = <IN>) );
        ($x_min,$x_max) = split(' ',$line);
        die "Error: Incorrect file format" unless (defined ($line = <IN>) );
        ($y_min,$y_max) = split(' ',$line);	
	close(IN);
	$YMAP_HOR = $XMAP_HOR*($y_max-$y_min+0.0)/(($x_max-$x_min+0.0)*cos(($y_max+$y_min+0.0)/2.0*$M_PI/180.0));
    }

    return if ($i == 0);
    
    if ($XMAP_VER == 0)
    {
	#my $n_sec = sprintf("%2d",$i);	
        #my $grd_in = "../../../TMP_files/vert/ver_11${n_sec}.grd";
	#open(IN,"<",$grd_in) or die "Can't open '$grd_in' : $!";
	#my $line;
        #die "Error: Incorrect file format" unless (defined ($line = <IN>) );    
        #die "Error: Incorrect file format" unless (defined ($line = <IN>) );
        #die "Error: Incorrect file format" unless (defined ($line = <IN>) );
        #($x_min,$x_max) = split(' ',$line);
        #die "Error: Incorrect file format" unless (defined ($line = <IN>) );
        #($y_min,$y_max) = split(' ',$line);	
	#close(IN);
	$XMAP_VER = ($x_max-$x_min)/($y_max-$y_min)*$YMAP_VER;
    }
}



main();
