import numpy as np
from matplotlib import pyplot as plt
import matplotlib
import os
import warnings
from glob import glob


# adjust distance from section parameter & max depth of section parameter in setver.dat file first

# also adjust area, model, iteration, and grid in model.dat file

# then execute ./PROGRAMS/3_VISUAL/_vis_ray_path/paths.exe

with open('model.dat','r') as f:
    info=f.read().splitlines()
f.close()

area=info[0]
model=info[1]
iteration=info[2]
grid=info[3]

print(area)

p_s=input("Enter 1 for P or 2 for S model: ")
sec_flag=input("Enter 1 for horizontal or 2 for vertical sections: ")
if(sec_flag=='1'):
    sec=input("Enter horizontal section number (plots only 1 section at a time)")
else:
    sec=input("Enter vertical section number or 'all' for sections in setver.dat: ")


warnings.filterwarnings("ignore",category=UserWarning)

if(sec=='all' and sec_flag=='2'):
    nodes_lst=[]
    evnts_lst=[]
    rays_lst=[]
    stations_lst=[]
    for flnm in glob('./TMP_files/rays/nodes_ver{gr:d}{ps:d}*.dat'.format(gr=np.int32(grid),ps=np.int32(p_s))):
        nodes_lst.append(np.loadtxt(flnm))
    for flnm in glob('./TMP_files/rays/ztr_ver*.dat'):
        evnts_lst.append(np.loadtxt(flnm))
    for flnm in glob('./TMP_files/rays/rays_ver{ps:d}*.dat'.format(ps=np.int32(p_s))):
        rays_lst.append(np.loadtxt(flnm))
    for flnm in glob('./TMP_files/rays/stat_ver*.dat'):
        stations_lst.append(np.loadtxt(flnm))
elif(sec!='all' and sec_flag=='2'):
    nodes_lst=[]
    evnts_lst=[]
    rays_lst=[]
    stations_lst=[]
    nodes_lst.append(np.loadtxt('./TMP_files/rays/nodes_ver{gr:d}{ps:d}{ver:d}.dat'.format(gr=np.int32(grid),ps=np.int32(p_s),ver=np.int32(sec)))) # nodes_ver/gr/ps/ver/.dat
    evnts_lst.append(np.loadtxt('./TMP_files/rays/ztr_ver{ver:d}.dat'.format(ver=np.int32(sec)))) # ztr_ver/ver/.dat
    rays_lst.append(np.loadtxt('./TMP_files/rays/rays_ver{ps:d}{ver:d}.dat'.format(ps=np.int32(p_s),ver=np.int32(sec))))# rays_ver/ps/ver/.dat
    stations_lst.append(np.loadtxt('./TMP_files/rays/stat_ver{ver:d}.dat'.format(ver=np.int32(sec)))) # stat_ver/ver/.dat

font = {'family' : 'sans-serif',
        'weight' : 'bold',
        'size'   : 16}

matplotlib.rc('font', **font)


if(sec_flag=='1'):
    fl_nodes='./TMP_files/rays/nodes_hor{gr:d}{ps:d}{ver:d}.dat'.format(gr=np.int32(grid),ps=np.int32(p_s),ver=np.int32(sec)) # nodes_ver/gr/ps/ver/.dat
    fl_evnts='./TMP_files/rays/ztr_hor{ver:d}.dat'.format(ver=np.int32(sec)) # ztr_ver/ver/.dat
    fl_rays='./TMP_files/rays/rays_hor{ps:d}{ver:d}.dat'.format(ps=np.int32(p_s),ver=np.int32(sec))# rays_ver/ps/ver/.dat
    fl_stations='./DATA/{ar:s}/inidata/stat_actual.dat'.format(ar=area) # stat_ver/ver/.dat

    print(fl_rays)

    lt_ln_arr=os.popen('awk -v lon_min=999 -v lat_min=999 -v lon_max=-999 -v lat_max=-999 \'{if($1>lon_max)lon_max=$1; if($2>lat_max)lat_max=$2; if($1<lon_min)lon_min=$1; if($2<lat_min)lat_min=$2}END{ print lon_min,lat_min,lon_max,lat_max}\' '+fl_rays).read().split()

    ln_min = lt_ln_arr[0]
    ln_max = lt_ln_arr[2]
    lat_min = lt_ln_arr[1]
    lat_max = lt_ln_arr[3]

    print(ln_min,ln_max,lat_min,lat_max)

    os.system('lon_min='+ln_min+';lon_max='+ln_max+';lat_min='+lat_min+';lat_max='+lat_max+';'
    + 'lat_annot=$(echo "scale=5;(${lat_max} - ${lat_min})/4" | bc -l);'
    + 'lat_anno_fine=$(echo "scale=5;(${lat_max} - ${lat_min})/8" | bc -l);'
    + 'lon_annot=$(echo "scale=5;(${lon_max} - ${lon_min})/4" | bc -l);'
    + 'lon_anno_fine=$(echo "scale=5;(${lon_max} - ${lon_min})/8" | bc -l);'
    + 'lat_ref=$(echo "scale=5;${lat_min} + (${lat_max} - ${lat_min})/7" | bc -l);'
    + 'lon_ref=$(echo "scale=5;(3.1*${lon_max} + ${lon_min})/4" | bc -l);'
    + 'mkdir ./PICS/ps/'+area+'/'+model+';'
    + 'PS1="./PICS/ps/'+area+'/'+model+'/'+'rays_ver_'+iteration+'_'+grid+'_'+p_s+'_'+sec+'.ps";'
    + 'echo Image_location: $PS1;'
    + 'REGION="${lon_min}/${lon_max}/${lat_min}/${lat_max}";'
    + 'SCALE_MAP="6i";'
    + 'SCALE_MAP_MATH=$(echo $SCALE_MAP | awk  \'{ string=substr($0, 0, length($0)-1); print string; }\');'
    + 'WIDTH_LEGEND=$(echo "scale=5;${SCALE_MAP_MATH}*(${lon_max} - ${lon_min})/4" | bc -l);'
    + 'HEIGHT_LEGEND=$(echo "scale=5;${SCALE_MAP_MATH}*(${lat_max} - ${lat_min})/6" | bc -l);'
    + 'GRID="/home/pratul/PackagesII/etopo/ETOPO1_Bed_g_gmt4.grd";'
    + 'MAP_SCALE_KM=$(echo "scale=1;(${lon_max} - ${lon_min})*100/8" | bc -l);'
    + 'echo $MAP_SCALE_KM;'
    + 'export HDF5_DISABLE_VERSION_CHECK=2;'
    + 'gmt gmtset MAP_FRAME_PEN 3;'
    + 'gmt gmtset MAP_FRAME_WIDTH 0.1;'
    + 'gmt gmtset MAP_FRAME_TYPE plain;'
    + 'gmt gmtset PS_PAGE_ORIENTATION=Landscape ;'
    + 'gmt gmtset PS_MEDIA=A4;'
    + 'gmt gmtset DOTS_PR_INCH 600;'
    + 'rm -f test.txt;'
    + 'gmt set MAP_FRAME_PEN = thickest,black;'
    + 'gmt set MAP_FRAME_TYPE = fancy;'
    + 'gmt set FONT_ANNOT_PRIMARY = 14p,Helvetica-Bold,black;'
    + 'gmt set FONT_LABEL = 14p,Helvetica-Bold,black;'
    + 'gmt psbasemap -R$REGION -Jm$SCALE_MAP -Bf${lon_anno_fine}a${lon_annot}/f${lat_anno_fine}a${lat_annot}:.:WeSN -V -K -Y2.0 -X2.0 > $PS1;'
    + 'gmt psxy '+fl_rays+' -R -J -O -K -i0,1 -Sc.02 -Ggrey -qi0:5: >> $PS1;'
    + 'gmt psxy '+fl_evnts+' -R -J -O -K -i0,1 -Sc.03 -Gred >> $PS1;'
    + 'gmt psxy '+fl_nodes+' -R -J -O -K -i0,1 -Ss.06 -Gblue >> $PS1;'
    + 'gmt psxy '+fl_stations+' -R -J -O -K -i0,1 -St.20 -Gdarkgreen >> $PS1;'
    + 'gmt pscoast -Jm$SCALE_MAP -R$REGION -W1p,black -N2p,0/0/0 -Dh -Lg${lon_ref}/${lat_ref}+c${lat_ref}+w${MAP_SCALE_KM}+ar+f -V -O >> $PS1;'
    + 'gmt psconvert $PS1 -A+m3c/3c/3c/3c -Tf;'
    + 'rm -f $PS1;')

else:

    for i in range(0,len(rays_lst)):

        rays = np.array(rays_lst[i])
        nodes = np.array(nodes_lst[i])
        evnts = np.array(evnts_lst[i])
        stations = np.array(stations_lst[i])

        x_max = np.max([np.max(rays[:,0]),np.max(rays[:,0]),np.max(rays[:,0]),np.max(rays[:,0])])
        x_min = np.min([np.min(rays[:,0]),np.min(rays[:,0]),np.min(rays[:,0]),np.min(rays[:,0])])
        y_max = np.max([np.max(rays[:,1]),np.max(rays[:,1]),np.max(rays[:,1]),np.max(rays[:,1])])
        y_min = np.min([np.min(rays[:,1]),np.min(rays[:,1]),np.min(rays[:,1]),np.min(rays[:,1])])

        plt.figure(figsize=(30,10),dpi=100)
        if(len(rays)>0):
            plt.scatter(rays[:,0],rays[:,1],1,'grey')
        else:
            print('Warning: No raypaths cross the current vertical section')

        if(len(nodes)>0):
            plt.scatter(nodes[:,0],nodes[:,1],15,'skyblue','s')
        else:
            print('Warning: No nodes found in the current vertical section')

        if(len(evnts)>0):
            plt.scatter(evnts[:,0],evnts[:,1],7,'red')
        else:
            print('Warning: No events found in the current vertical section')

        if(len(stations) > 0):
            plt.scatter(stations[:,0],stations[:,1]+1,50,'green','^')
        else:
            print('Warning: No stations found in the current vertical section')

        print('Note: Ray paths crossing the vertical section also shown')

        plt.ylim([y_min,y_max])
        plt.xlim([x_min,x_max])
        ax = plt.gca()
        ax.set_aspect('equal', adjustable='box')
        plt.minorticks_on()
        plt.xlabel('Distance along the profile (in km)',fontsize=16,fontweight='bold')
        plt.ylabel('Depth (in km)',fontsize=16,fontweight='bold')
        plt.legend(['Rays','Nodes','Events','Stations'])
        os.system('mkdir ./PICS/ps/%s/%s >/dev/null 2>&1' % (area,model))
        plt.savefig('./PICS/ps/{ar:s}/{md:s}/rays_ver_{it:d}_{gr:d}_{ps:d}_{ver:s}.png'.format(ar=area,
        md=model,it=np.int32(iteration),gr=np.int32(grid),ps=np.int32(p_s),ver=sec),bbox_inches='tight')
        plt.show()
        plt.close()
