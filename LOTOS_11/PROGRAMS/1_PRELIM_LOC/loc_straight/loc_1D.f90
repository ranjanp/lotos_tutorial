character*8 ar,md,line
real dx_it(10),dy_it(10),dz_it(10)
real res_it1(10),res_it2(10),wps_it(10)
integer kodes(10,20000)
real gkode(20000)
real xvert1(20),yvert1(20),xvert2(20),yvert2(20)
character*2 ver
real xmark(200,20),ymark(200,20),smark(200,20)
integer nmark(100),ngood(500)
character*5 stacod(500),stac


common/refmod/nref,href(600),vref(2,600)

common/pi/pi,per
common/krat/nkrat,istkr(500),ipskr(500),ndrkr(500),tobkr(500),trfkr(500)
common/stations/nst,xstat(500),ystat(500),zstat(500)

common/mod_2d/xmod1,nxmod,dxmod,ymod1,nymod,dymod, dv_mod(300,300)
common/ray_param/ds_ini,ds_part_min,bend_min0,bend_max0
common/loc_param/res_loc1,res_loc2,w_P_S_diff
common/ray/ nodes,xray(1000),yray(1000),zray(1000)


one=1.e0
pi=asin(one)*2.e0
per=pi/180.e0

res_loc1=0.
res_loc2=50./1000.
w_P_S_diff=10

nxmod=0


open(1,file='../../../model.dat')
read(1,'(a8)')ar
read(1,'(a8)')md
close(1)

write(*,*)' Preliminary location of sources using straight lines in the starting model'
write(*,*)' ar=',ar,' md=',md


!******************************************************************
key_ft1_xy2=1
open(1,file='../../../DATA/'//ar//'/'//md//'/MAJOR_PARAM.DAT')
do i=1,10000
    read(1,'(a8)',end=513)line
    if(line.eq.'GENERAL ') goto 514
end do
513 continue
write(*,*)' cannot find GENERAL INFORMATION in MAJOR_PARAM.DAT!!!'
pause
514 continue
read(1,*)k_re1_syn2
read(1,*)
read(1,*)koe
read(1,*)
read(1,*,end=441,err=441)key_ft1_xy2
read(1,*,end=441,err=441)key_true1
441 close(1)
!******************************************************************

open(1,file='../../../DATA/'//ar//'/'//md//'/MAJOR_PARAM.DAT')
do i=1,10000
	read(1,'(a8)',end=553)line
	if(line.eq.'LOC_PARA') goto 554
end do
553 continue
write(*,*)' cannot find TRACE PARAMETERS in MAJOR_PARAM.DAT!!!'
pause
554 continue
read(1,*)
read(1,*)ds_ini
read(1,*)ds_segm_min
read(1,*)bend_min0
read(1,*)bend_max0
close(1)
!******************************************************************


w_qual=1
open(1,file='../../../DATA/'//ar//'/'//md//'/MAJOR_PARAM.DAT')
do i=1,10000
	read(1,'(a8)',end=543)line
	if(line.eq.'LIN_LOC_') goto 544
end do
543 continue
write(*,*)' cannot find LIN_LOC_PARAM in MAJOR_PARAM.DAT!!!'
pause

544 continue
read(1,*)krat_min
read(1,*)dist_stat
read(1,*)wgs
read(1,*)dist_limit	!=100
read(1,*)n_pwr_dist	!=1
read(1,*)ncyc_av	!=10
read(1,*)
read(1,*)	! For output:
read(1,*)bad_max	!=30
read(1,*)res_1_km
read(1,*)sss_max
read(1,*)
read(1,*)nfreq_print
!nfreq_print=1
read(1,*)
read(1,*)niter_loc
do it=1,niter_loc
	read(1,*)
	read(1,*)dx_it(it),dy_it(it),dz_it(it)
	!write(*,*)dx_it(it),dy_it(it),dz_it(it)
	read(1,*)res_it1(it)
	read(1,*)res_it2(it)
	read(1,*)wps_it(it)
end do
close(1)

call read_z_lim(ar,md)
call read_vref(ar,md)
call read_topo(ar)

!xm=5; ym=5; zm=5; ips=2
!vvv=velocity (xm,ym,zm,ips)
!write(*,*)' vvv=',vvv
!stop

i=system('mkdir ..\..\..\TMP_files\loc')
i=system('mkdir ..\..\..\PICS\'//ar//'\'//md//'\LOC')

xstart=0; ystart=0; zstart=0
k_star_po=0
open(1,file='../../../DATA/'//ar//'/'//md//'/MAJOR_PARAM.DAT')
do i=1,10000
	read(1,'(a8)',end=643)line
	if(line.eq.'START_PO') goto 644
end do
643 continue
!write(*,*)' cannot find START_POINT in MAJOR_PARAM.DAT!!!'
goto 645

644 continue
k_star_po=1
read(1,*)xstart,ystart,zstart 
645 close(1) 


key_info=0
open(3,file='../../../DATA/'//ar//'/inidata/event_info.dat',status='old',err=46)
key_info=1
close(3)
46  continue


if(key_ft1_xy2.eq.2) then
 
    open(1,file='../../../DATA/'//ar//'/inidata/stat_xy.dat')
    open(11,file='../../../DATA/'//ar//'/'//md//'/data/stat_xy.dat')
    nst=0
    133	read(1,*,end=144)xst,yst,zst
	    write(11,*)xst,yst,zst
	    nst=nst+1
	    xstat(nst)=xst
	    ystat(nst)=yst
	    zstat(nst)=zst
	    goto 133
    144	close(1)
    close(11)
    write(*,*)' nst=',nst

else if(key_ft1_xy2.eq.1) then

    open(1,file='../../../DATA/'//ar//'/'//md//'/MAJOR_PARAM.DAT')
    do i=1,10000
	    read(1,'(a8)',end=593)line
	    if(line.eq.'AREA_CEN') goto 594
    end do
    593 continue
    write(*,*)' cannot find AREA CENTER in MAJOR_PARAM.DAT!!!'
    pause

    594 read(1,*)fi0,tet0
    !write(*,*)fi0,tet0
    close(1)

    ! Read the coordinates of the stations
    open(1,file='../../../DATA/'//ar//'/inidata/stat_ft.dat')
    open(12,file='../../../DATA/'//ar//'/'//md//'/data/stat_xy.dat')
    nst=0
    33	    continue
            if(key_info.eq.0)read(1,*,end=44)fi,tet,zst
            if(key_info.eq.1)read(1,*,end=44)fi,tet,zst,stac
	    call SFDEC(fi,tet,0.,X,Y,Z,fi0,tet0)
	    nst=nst+1
	    xstat(nst)=x
	    ystat(nst)=y
	    zstat(nst)=zst
            if(key_info.eq.1)stacod(nst)=stac
	    write(12,*)xstat(nst),ystat(nst),zstat(nst)
	    !write(*,*)xstat(nst),ystat(nst),zstat(nst)
	    goto 33
    44	close(12)
    close(1)
else
    write(*,*)' key_ft1_xy2=',key_ft1_xy2
    stop
end if
write(*,*)' nst=',nst

k_format=1

9092 continue

if(k_re1_syn2.eq.1) then
    open(1,file='../../../DATA/'//ar//'/inidata/rays.dat')
    write(*,*)' ******************************************'
    write(*,*)' REAL DATA INVERSION'
else if(k_re1_syn2.eq.2) then
	write(*,*)' ******************************************'
	write(*,*)' SYNTHETIC DATA INVERSION'
	open(1,file='../../../DATA/'//ar//'/'//md//'/data/rays_syn.dat')
else
	write(*,*)' Synthetic key in MAJOR_PARAM is not defined correctly: k_re1_syn2=',k_re1_syn2
	stop
end if

open(11,file='../../../DATA/'//ar//'/'//md//'/data/rays0.dat',form='unformatted')
open(41,file='../../../DATA/'//ar//'/'//md//'/data/resid0.dat')
open(12,file='../../../DATA/'//ar//'/'//md//'/data/srce_0.dat')
open(14,file='../../../DATA/'//ar//'/'//md//'/data/srce_old.dat')

!write(*,*)' key_info=',key_info

nzt=0; nztgood=0; nrp=0; nrs=0; nray=0 
errtot=0; nerr=0


228	continue
    if(key_re1_syn2.eq.1) then
        read(1,*,end=229)fini,tini,zini,nkrat,iyr,imt,idy,ihr,imn,sec,uidx
    else
        read(1,*,end=229)fini,tini,zini,nkrat,uidx
    end if
    !write(*,*)fini,tini,zini,nkrat
    if(k_format.eq.1) then
        read(1,'(i5,4i3,f7.3,f4.1)',err=9090)iyr,imt,idy,ihr,imn,sec,amag
        !write(*,*)' date=', iyr,imt,idy,ihr,imn,sec,amag
    else
        iyr=9999; imt=99; idy=99; ihr=99; imn=99; sec=99.9999; amag=9.9999
        !write(*,*)' no date'
    end if
    if(nkrat.eq.0) goto 228
    do ikr=1,nkrat
        read(1,*,err=9090)ips,ist,tobs
        !write(*,*)' ips,ist,tobs=',ikr,ips,ist,tobs
        if(ips.ne.1.and.ips.ne.2) goto 9090
        ipskr(ikr)=ips; istkr(ikr)=ist; tobkr(ikr)=tobs
    end do
    goto 9091
9090 continue
     close(1);close(11); close(12); close(14); close(41)
     k_format=2
     write(*,*)' Change the format: k_format=',k_format
     !pause
     goto 9092
9091 continue
    nzt=nzt+1
    if(koe.eq.1.and.mod(nzt,2).eq.0) goto 228
    if(koe.eq.2.and.mod(nzt,2).eq.1) goto 228
    if(nkrat.lt.krat_min) goto 228
    !if(nzt.lt.46) goto 228

    
    
    
    ztopo=relief_surf(fini,tini)
    !write(*,*)' zini=',zini,' ztopo=',ztopo
    if(zini.lt.ztopo) zini=ztopo+0.01
    
    
    

    !write(*,*)fini,tini,zold,nkrat
    call SFDEC(fini,tini,0.,xini,yini,Z,fi0,tet0)
    xold=xini; yold=yini; zold=zini
    xmax=xini; ymax=yini; zmax=zini
    !write(*,*)' xini=',xini,' yini=',yini
    !write(*,*)' fini=',fini,' tini=',tini
    
    dismin=9999999
    do i=1,nkrat
        ist=istkr(i); xs=xstat(ist); ys=ystat(ist); zs=zstat(ist)
        hordist=sqrt((xs-xini)*(xs-xini)+(ys-yini)*(ys-yini))
        if(hordist.lt.dismin) dismin=hordist
    end do
    !write(*,*)' xini=',xini,' yini=',yini,' dismin=',dismin
    !stop
    if(dismin.gt.dist_stat) goto 228

    do iter=1,niter_loc

        res_loc1=res_it1(iter); res_loc2=res_it2(iter)
        w_P_S_diff=wps_it(iter)
        dx_loc=dx_it(iter); dy_loc=dy_it(iter); dz_loc=dz_it(iter)

!        write(*,*)xmax,ymax,zmax
!        do ikr=1,nkrat
!            write(*,*)istkr(ikr),ipskr(ikr),tobkr(ikr)
!        end do
        call goal_fun_lin(xmax,ymax,zmax, aver,goal)
        !write(*,*)' iter=',iter,' goal=',goal
        nkode=1 
        kodes(1,nkode)=0; kodes(2,nkode)=0; kodes(3,nkode)=0
        gmax=goal; gkode(nkode)=gmax
        ixmax1=0; iymax1=0; izmax1=0
        icycle=0

        282 continue
        index=0
        do iix=1,5
            ix=ixmax1+iix-3; dx=dx_loc*ix
            do iiy=1,5
                iy=iymax1+iiy-3; dy=dy_loc*iy
                call decsf(xmax+dx,ymax+dy,0.,fi0,tet0,fff,ttt,h)
                zlim=z_lim(fff,ttt)
                zlim_up=relief_surf(fff,ttt)
                !write(*,*)' ix=',ix,' iy=',iy
                !write(*,*)' fff=',fff,' ttt=',ttt,' zlim_up=',zlim_up
                do iiz=1,5
                    iz=izmax1+iiz-3; dz=dz_loc*iz
                    if(zmax+dz.gt.zlim) cycle
                    if(zmax+dz.lt.zlim_up) cycle

                    if(nkode.ne.0) then
                        do ik=1,nkode
                            if(kodes(1,ik).eq.ix.and.kodes(2,ik).eq.iy.and.kodes(3,ik).eq.iz) goto 281
                        end do
                    end if
                    !write(*,'(3i3,3f6.1,f7.3)')ix,iy,iz,xmax+dx,ymax+dy,zmax+dz

                    call goal_fun_lin(xmax+dx,ymax+dy,zmax+dz, aver,goal)

                    !write(*,'(3i3,3f6.1,f7.3)')ix,iy,iz,xmax+dx,ymax+dy,zmax+dz,goal
                    !write(*,*)' goal=',goal

                    nkode=nkode+1
                    if(nkode.gt.2000) goto 228
                    kodes(1,nkode)=ix; kodes(2,nkode)=iy; kodes(3,nkode)=iz
                    gkode(nkode)=goal

                    if(goal.le.gmax) cycle
!write(*,*)xmax+dx,ymax+dy,zmax+dz,' topo=',zlim_up
                    index=1; ixmax=ix; iymax=iy; izmax=iz; gmax=goal
                    !write(*,*)ixmax,iymax,izmax,' goal=',goal
281		     continue
                end do
            end do
        end do
        icycle=icycle+1

        !write(*,*)icycle,ixmax,iymax,izmax,gmax

        if(index.eq.1) then
            ixmax1=ixmax; iymax1=iymax; izmax1=izmax
            goto 282
        end if

        xmax=xmax+dx_loc*(ixmax1)
        ymax=ymax+dy_loc*(iymax1)
        zmax=zmax+dz_loc*(izmax1)

        !write(*,*)' after iteration:',iter
        !write(*,*)' x=',xmax,' y=',ymax,' z=',zmax,' g=',gmax
    end do

    xzt=xmax; yzt=ymax; zzt=zmax

   
    
    
    call goal_fun_lin(xold,yold,zold, aver,gold)
    call goal_fun_lin(xzt,yzt,zzt, aver,goal)
           ! write(*,*)' old:',xold,yold,zold,gold
            !write(*,*)' new:',xzt,yzt,zzt,goal

    nbad=0
    ngood=1
    dismin=9999999
    do i=1,nkrat
        tobkr(i)=tobkr(i)-aver
        ist=istkr(i); xs=xstat(ist); ys=ystat(ist); zs=zstat(ist)
        dist=sqrt((xs-xzt)*(xs-xzt)+(ys-yzt)*(ys-yzt)+(zs-zzt)*(zs-zzt))
        hordist=sqrt((xs-xzt)*(xs-xzt)+(ys-yzt)*(ys-yzt))
        if(hordist.lt.dismin) dismin=hordist
        if(dist.gt.sss_max) dist=sss_max
        res_limit=dist*res_1_km
        if(ipskr(i).eq.2)res_limit=res_limit*wgs
        dt=tobkr(i)-trfkr(i)
        !write(*,*)ipskr(i),istkr(i),dt,res_limit
        if(abs(dt).lt.res_limit) cycle
        ngood(i)=0
        nbad=nbad+1
    end do
    !stop

    nk=nkrat-nbad
    abad=nbad
    akrat=nkrat
    ratio_bad=(abad/akrat)
    !write(*,*)' nk=',nk,' nkrat=',nkrat

    !write(*,*)' nbad=',nbad,' ngood=',nk
    !pause
    if(ratio_bad*100.gt.bad_max) goto 228
    if(nk.lt.krat_min) goto 228
    if(dismin.gt.dist_stat) goto 228


    call decsf(xzt,yzt,0.,fi0,tet0,fzt,tzt,h)
    
    !ztopo=relief_surf(fzt,tzt)
    !ztopo0=relief_surf(fini,tini)
    !if(ztopo.gt.zzt) then
    !    write(*,*)' nzt=',nzt
    !    write(*,*)' xzt=',xzt,' yzt=',yzt,' zzt=',zzt
    !    write(*,*)' fzt=',fzt,' tzt=',tzt
    !    write(*,*)' fini=',fini,' tini=',tini
    !    write(*,*)' ztopo0=',ztopo0,' ztopo=',ztopo
    !    stop
    !end if

    write(12,*)fzt,tzt,zzt,uidx
    write(14,*)fini,tini,zini,uidx
    err=sqrt((xzt-xini)**2+(yzt-yini)**2+(zzt-zini)**2)
    
    errtot=errtot+err
    nerr=nerr+1

    write(11)xzt,yzt,zzt,nk,uidx
 	!write(11,'(i5,4i3,f7.3,f4.1)')iyr,imt,idy,ihr,imn,sec,amag
    write(41,*)xzt,yzt,zzt,nk,uidx
    do i=1,nkrat
        if(ngood(i).eq.0) cycle
        write(11)ipskr(i),istkr(i),tobkr(i),trfkr(i)
       write(41,*)ipskr(i),istkr(i),tobkr(i)-trfkr(i)
        dtot=dtot+abs(tobkr(i)-trfkr(i))
        nray=nray+1
        if(ipskr(i).eq.1) nrp=nrp+1
        if(ipskr(i).eq.2) nrs=nrs+1
    end do

    nztgood=nztgood+1

    !nfreq_print=1
    if(mod(nztgood,nfreq_print).eq.0) then
        write(*,488)nzt,xold,yold,zold,gold
        write(*,489)nztgood,xzt,yzt,zzt,goal
        488	format(i6,' Old: x=',f8.2,' y=',f8.2,' z=',f8.2,' ank=',f7.2)
        489	format(i6,' New: x=',f8.2,' y=',f8.2,' z=',f8.2,' ank=',f7.2)
        dcur=dtot/nray
        write(*,*)' nkrat=',nkrat,' nk=',nk,' nray=',nray
        write(*,*)
    end if

    goto 228
229 close(1)
close(11)
close(12)
close(14)
close(41)
if(k_re1_syn2.eq.1) close(2)
if(k_re1_syn2.eq.1) close(21)

errtot=errtot/nerr

write(*,*)' nztgood=',nztgood,' nray=',nray
write(*,*)' nrp=',nrp,' nrs=',nrs
if(k_re1_syn2.eq.2) write(*,*)' Error of source location =',errtot,' km'


stop
end
